
-- hds interface_end

------------------------------------------------------
-- FIFO pointer management for fifo_sync component
-- $Log: synth.vhd,v $
-- Revision 1.1  2012/06/26 14:00:33  pschirrer
-- Initial version
--
-- Revision 1.8  2008/10/20 08:55:07  igehin
-- + RF engine :
-- - re-enabled Type B Frame Guard Time error check in reception to comply  with RCTIF test
-- - TR1 timing checks modified 02/04/2008 upon A. Maurel request for RCTIF test :   
--   10 ETUs < TR1 < 25 ETUs
-- - reception engine changed to allow 1/8 ETUs resolution
-- 
-- + SAM ART : Set to  2ms instead of 200 us for time 't2': patched on 23/01/2008 to reflect calypso changes.
--   See  � #005 from calypso technical note from Spirtech on 30 April 2002
--
-- Revision 1.7  2003/06/17 12:55:22  doziere
-- Addition of the synchronous FIFOs 
-- (transmission and reception)
--
-- Revision 1.5  2002/06/28 11:08:18  doziere
-- FIFO Changed with generic
-- CmdSize becomes unsigned
-- Rspsize becomes unsigned and its management size
-- CollError for manchester type is different
-- TR0 and TR1 changed according to the VBTP
-- the cpt5ms is OK
-- SOF & EOF is different for Type Bp for TX
-- anticollision for manchester type is OK
--
-- Revision 1.4  2002/05/21 07:48:46  gehin
-- Replaced X00 with (others=>'0') for returning a null value when fifo is empty.
--
-- Revision 1.3  2002/04/29 11:52:34  gehin
-- FIFO now returns null value (0) if empty.
--
-- Revision 1.2  2002/04/29 11:51:46  gehin
-- FIFO now returns null value (0) if empty.
--
-- Revision 1.1  2002/04/17 09:44:44  gehin
-- Pointer management for <shared_lib.fifo_sync>.
--
-- $Revision: 1.1 $
------------------------------------------------------

ARCHITECTURE synth OF fifo_sync_pointers IS

   constant    cResetVector       : std_logic_vector := std_logic_vector(to_unsigned(gConf.reset_polarity,1));
   constant    cReset             : std_logic := cResetVector(0);

   signal      sRdpointer         : unsigned(gConf.numwords_width-1 downto 0); -- read pointer
   signal      sWrpointer         : unsigned(gConf.numwords_width-1 downto 0); -- write pointer
   signal      sWord_count        : unsigned(CounterNumWords'HIGH downto 0); -- amount of words actually not read ans stored in FIFO

--   signal      sEmptyDelayed     : std_logic; -- 1 clock delayed empty flag
   signal      sEmpty            : std_logic; -- '1' when FIFO is empty , i.e sWord_count is 0.
   signal      sFull             : std_logic; -- '1' when FIFO is full , i.e sWord_count is equal to numwords
--   signal      sReadEnable      : std_logic;
BEGIN

-- set module ouputs

   writable <= '1' ; -- actually always '1' because RAM write access time is 1 clock period.
   readable <= '1'; -- actually always '1' ,  but at least 1 wait state (i.e 1 master clock tick) is NECESSARY after a read request.

   empty  <= sEmpty;
   full   <= sFull;

   CS          <= '1'; --RAM is always selected
   WRCYCLE     <= wrreq    when sFull='0' else '0';            -- writing to RAM is allowed only when FIFO is not full
   q           <= DataOut  when sEmpty='0' else (others => '0'); -- force FIFO to return 0 if FIFO is empty
   rd_addr     <= std_logic_vector(unsigned(sRdpointer));      -- RAM read address pointer
   wr_addr     <= std_logic_vector(unsigned(sWrpointer));      -- RAM write address pointer
   CounterNumWords <= std_logic_vector(unsigned(sWord_count)); -- output word counter

-- pointers management

process(reset,CLK)
begin

	if reset= cReset then -- asynchronous reset
		sEmpty      <= '1'; -- FIFO is empty
		sFull       <= '0'; -- FIFO is not full
		sRdpointer  <= (others=>'0'); -- read pointer set to location 0
		sWrpointer  <= (others=>'0'); -- write pointer set to location 0
		sWord_count <= (others=>'0'); -- 0 word stored

	elsif Rising_Edge(CLK) then

		if rdreq='1' and sEmpty='0' then -- read request allowed if FIFO is not empty
			-- ring pointer
			if sRdpointer = gConf.numwords-1 then -- increment ring pointer and reset it if overflow reached
				sRdpointer <= (others=>'0');
			else
				sRdpointer <= sRdpointer+1;
			end if;
			-- word counter
			if wrreq='0' or sFull='1' then
				sFull <='0';
				sWord_count <= sWord_count-1; -- if no concurrent write then decrement word counter
				if sWord_count=1 then -- need to anticipate 1 clock tick to set empty flag when count reach 0
					sEmpty <='1'; -- set empty flag
				else
					sEmpty <='0'; -- set empty flag
				end if;
			end if;
		end if;

		if wrreq='1' and sFull='0' then -- write request allowed if FIFO is not full
			-- ring pointer
			if sWrpointer = gConf.numwords-1 then -- increment ring pointer and reset it if overflow reached
				sWrpointer <= (others=>'0');
			else
				sWrpointer <= sWrpointer+1;
			end if;
			-- word counter
			if rdreq='0' or sEmpty='1' then -- if no concurrent read then increment word counter
				sEmpty <='0';
				sWord_count <= sWord_count+1;
				if sWord_count=gConf.numwords-1 then -- need to anticipate 1 clock tick to set full flag when count reach numwords
					sFull <='1'; -- set full flag
				else
					sFull <='0'; -- set full flag
				end if;
			end if;
		end if;
	end if;
end process;
END synth;

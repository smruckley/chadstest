
-- hds interface_end
architecture synth of com_uart_timeout is

   signal   sCnt           : unsigned(11 downto 0); -- counter to detect time-out condition
   signal   sCntFree       : unsigned(8 downto 0) ; -- counter to detect activity on serial line, sCntFree is used to build a "retriggerable monostable"  
   signal   sCntEnable     : std_logic;
   signal   sLineFree      : std_logic;
   
begin
   process (nRESET,CLK_COM)
      begin
      if nRESET='0' then
         sCnt        <= (others=>'0');
         sCntFree    <= (others=>'0');
         TimeOut     <= '0';
         sCntEnable  <= '0';
         sLineFree    <= '1';
      elsif rising_edge(CLK_COM) then

         TimeOut <= '0';
         
         if StartDetected='1' then  -- start bit detected on serial line
            sLineFree <= '0';
            if divider = '0' then
               sCntFree <= "000111100"; -- duration is by default 15 time bit x4 (boost mode)
            else
               sCntFree <= "011110000"; -- duration is by default 15 time bit x16 (because tick period is 16 times baud rate period)
            end if;
         elsif Tick='1' and sLineFree='0' then 
            sCntFree <= sCntFree-1;
            if sCntFree=0 then
               sLineFree <= '1';
            end if;
         end if;

         if DataReceived='1' or GetNextData='1' then -- restart counting triggered by storing or getting data from RX FIFO
            sCnt <= unsigned(TimeOutDur)& "0000";
            sCntEnable <= '1';
         elsif Tick='1' and sCntEnable='1' then 
            sCnt <= sCnt-1;
            if sCnt=0 then
               TimeOut <= not fifo_rx_empty_lat; -- generate timeout only if RX FIFO is not empty
               sCntEnable <= '0';
            end if;
         end if;
      end if;
      
   end process;
   
   LineFree <= sLineFree;
   
end synth;


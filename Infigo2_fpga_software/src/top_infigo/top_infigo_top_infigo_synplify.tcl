#-- Lattice Semiconductor Corporation Ltd.
#-- Synplify OEM project file

#device options
set_option -technology MACHXO2
set_option -part LCMXO2_2000HC
set_option -package TG144I
set_option -speed_grade -4

#compilation/mapping options
set_option -symbolic_fsm_compiler true
set_option -resource_sharing true

#use verilog 2001 standard option
set_option -vlog_std v2001

#map options
set_option -frequency 200
set_option -fanout_limit 100
set_option -auto_constrain_io true
set_option -disable_io_insertion false
set_option -retiming false; set_option -pipe false
set_option -force_gsr false
set_option -compiler_compatible true
set_option -dup false
set_option -frequency 1
set_option -default_enum_encoding default

#simulation options


#timing analysis options
set_option -num_critical_paths 3
set_option -num_startend_points 0

#automatic place and route (vendor) options
set_option -write_apr_constraint 0

#synplifyPro options
set_option -fixgatedclocks 3
set_option -fixgeneratedclocks 3
set_option -update_models_cp 0
set_option -resolve_multiple_driver 1

#-- add_file options
add_file -vhdl {D:/lscc/diamond/2.0/cae_library/synthesis/vhdl/machxo2.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/PACK-shared_lib-pkg_defines-_package.vhd}
add_file -vhdl -lib "lattice" {D:/fpga/Infigo2/30Oct2012/PACK-lattice-components-_package.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-imx_bridge-synth_fe.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/UNIT-shared_lib-com_uart_tx_div-fsm.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-com_uart_timeout-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-xmit_seq-fsm.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-com_uart_rx_div-fsm.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-regs_uart_fifo-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-ram_sync_dp-synth_ram_sync_dp_generic.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-fifo_sync_pointers-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-fifo_sync-struct.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-uart_fifo_modem-struct.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/UNIT-shared_lib-com_uart7816_tx-fsm.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/UNIT-shared_lib-com_uart7816_counter-synth.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/UNIT-shared_lib-com_uart7816_rx-fsm.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/UNIT-shared_lib-com_uart7816_clkgen-synth.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/UNIT-shared_lib-com_uart7816_regs-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-com_uart7816-struct.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-intefb-structure.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-powctrl-structure.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-intosc-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/UNIT-infigo2-pwm_io-synth.vhd}
add_file -vhdl -lib "work" {D:/fpga/Infigo2/30Oct2012/UTOP-infigo2-top_infigo-struct.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-imx_bridge-synth_fe.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/VIEW-shared_lib-com_uart_tx_div-fsm.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-com_uart_timeout-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-xmit_seq-fsm.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-com_uart_rx_div-fsm.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-regs_uart_fifo-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-ram_sync_dp-synth_ram_sync_dp_generic.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-fifo_sync_pointers-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-fifo_sync-struct.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-uart_fifo_modem-struct.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/VIEW-shared_lib-com_uart7816_tx-fsm.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/VIEW-shared_lib-com_uart7816_counter-synth.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/VIEW-shared_lib-com_uart7816_rx-fsm.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/VIEW-shared_lib-com_uart7816_clkgen-synth.vhd}
add_file -vhdl -lib "shared_lib" {D:/fpga/Infigo2/30Oct2012/VIEW-shared_lib-com_uart7816_regs-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-com_uart7816-struct.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-intefb-structure.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-powctrl-structure.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-intosc-synth.vhd}
add_file -vhdl -lib "infigo2" {D:/fpga/Infigo2/30Oct2012/VIEW-infigo2-pwm_io-synth.vhd}
add_file -vhdl -lib "work" {D:/fpga/Infigo2/30Oct2012/VTOP-infigo2-top_infigo-struct.vhd}

#-- top module name
set_option -top_module top_infigo

#-- set result format/file last
project -result_file {D:/fpga/Infigo2/30Oct2012/top_infigo/top_infigo_top_infigo.edi}

#-- error message log file
project -log_file {top_infigo_top_infigo.srf}

#-- set any command lines input by customer


#-- run Synplify with 'arrange HDL file'
project -run hdl_info_gen -fileorder
project -run

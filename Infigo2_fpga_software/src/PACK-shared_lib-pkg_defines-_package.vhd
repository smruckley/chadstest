PACKAGE pkg_defines IS


   type rConf_C_less_Per_sam is
      record
	ETU_divider	: integer;
	CLK4M52_divider	: integer;
        techno         	: integer;
      end record;

   constant  cConf_C_less_Per_sam : rConf_C_less_Per_sam  :=
      (
	ETU_divider	=> 37,
	CLK4M52_divider	=> 7,
         techno         =>  1 -- select RAM techno (1=generic implementation)
      );

   type cShared_lib_Cpu_bridge_at91_Csm is array (0 to 19) of integer; -- maximum chip select number is 20
   
   type rConf_Shared_lib_Cpu_bridge_at91 is
      record
         ADDR_SIZE    : integer   ;  -- size of all address buses (CPU and modules)
         DATA_SIZE    : integer   ;  -- size of all data buses (CPU and modules)
         USE_CSM      : cShared_lib_Cpu_bridge_at91_Csm; -- array of integer used to selectively generate chip select logic. 
                                                         -- if USE_CSM(n)=1 then chip select n will be implemented.
                                                         -- if USE_CSM(n)=0 then chip select n will not be implemented (this will save associated logic gates).
      end record;

   -- default init values for record parameters
   
   constant  cConf_Shared_lib_Cpu_bridge_at91: rConf_Shared_lib_Cpu_bridge_at91  :=
      (
         ADDR_SIZE    => 8, 
         DATA_SIZE    => 8,
         USE_CSM      => (others=>0) -- default is no chip select used
      );
      
-- infigo2.uart_fifo_modem imported from 'd:\user\projet\hdl\renoir\work\design_libs/infigo2/uart_fifo_modem/pkg.conf'
   -- $Revision: 1.1 $
   
   type rConf_infigo2_uart_fifo_modem is
      record
         numwords                : integer;
         numwords_width          : integer;
		 FifoLvl1                : integer;
         techno_ram              : integer;   
         techno_optimized_fifo   : integer; -- used to select an architecture optimized for a specific technology
      end record;

   constant  cConf_infigo2_uart_fifo_modem : rConf_infigo2_uart_fifo_modem  :=
      (
         numwords                =>  256,
         numwords_width          =>  8,
		 FifoLvl1                =>  8,
         techno_ram              =>  1, -- generic implementation for RAM 
         techno_optimized_fifo   =>  1  -- generic implentation for FIFO
      );
      
      
-- shared_lib.com_uart7816 imported from 'd:\user\projet\hdl\renoir\work\design_libs/shared_lib/com_uart7816/pkg.conf'
   -- $Revision: 1.1 $
   
   type rConf_Shared_lib_Com_uart7816 is
      record
         numwords                : integer;
         numwords_width          : integer;
         techno_ram              : integer;   
         techno_optimized_fifo   : integer; -- used to select an architecture optimized for a specific technology
         enable_tristate         : integer;
      end record;

   constant  cConf_Shared_lib_Com_uart7816 : rConf_Shared_lib_Com_uart7816  :=
      (
         numwords                =>  256,
         numwords_width          =>  8,
         techno_ram              =>  1, -- generic implementation for RAM 
         techno_optimized_fifo   =>  1, -- generic implentation for FIFO
         enable_tristate         =>  0  -- 1 to enable tristae on RX_TX line  
      );
      
-- shared_lib.com_uart_fifo imported from 'd:\user\projet\hdl\renoir\work\design_libs/shared_lib/com_uart_fifo/pkg.conf'

   -- $Revision: 1.1 $
   
   type rConf_Shared_lib_Com_uart_fifo is
      record
         gTechno         : integer;   
      end record;

   constant  cConf_Shared_lib_Com_uart_fifo : rConf_Shared_lib_Com_uart_fifo  :=
      (
         gTechno         =>  1 -- select RAM techno (1=generic implementation)
      );
      
-- shared_lib.com_uart_fifo_rx imported from 'd:\user\projet\hdl\renoir\work\design_libs/shared_lib/com_uart_fifo_rx/pkg.conf'
   -- $Revision: 1.1 $
   
   type rConf_Shared_lib_Com_uart_fifo_rx is
      record
         numwords                : integer;
         numwords_width          : integer;
         techno_ram              : integer;   
         techno_optimized_fifo   : integer; -- used to select an architecture optimized for a specific technology
      end record;

   constant  cConf_Shared_lib_Com_uart_fifo_rx : rConf_Shared_lib_Com_uart_fifo_rx  :=
      (
         numwords                =>  256,
         numwords_width          =>  8,
         techno_ram              =>  1, -- generic implementation for RAM 
         techno_optimized_fifo   =>  1  -- generic implentation for FIFO
      );
      
      
-- shared_lib.com_uart_modem_boost_fifo imported from 'd:\user\projet\hdl\renoir\work\design_libs/shared_lib/com_uart_modem_boost_fifo/pkg.conf'
   -- $Revision: 1.3 $
   
   type rConf_Shared_lib_Com_uart_modem_boost_fifo is
      record
         numwords                : integer;
         numwords_width          : integer;
         techno_ram              : integer;   
         techno_optimized_fifo   : integer; -- used to select an architecture optimized for a specific technology
      end record;

   constant  cConf_Shared_lib_Com_uart_modem_boost_fifo : rConf_Shared_lib_Com_uart_modem_boost_fifo  :=
      (
         numwords                =>  256,
         numwords_width          =>  8,
         techno_ram              =>  1, -- generic implementation for RAM 
         techno_optimized_fifo   =>  1  -- generic implentation for FIFO
      );
      
-- shared_lib.com_uart_rx_div imported from 'd:\user\projet\hdl\renoir\work\design_libs/shared_lib/com_uart_rx_div/pkg.conf'
-- $Revision: 1.3 $

   
   type rConf_Shared_lib_Com_uart_rx_div is
      record
         NDBits         : integer; -- number of data bits
      end record;

   constant  cConf_Shared_lib_Com_uart_rx_div: rConf_Shared_lib_Com_uart_rx_div  :=
      (
         NDBits         => 8
      );
      
   
      
-- shared_lib.fifo_sync imported from 'd:\user\projet\hdl\renoir\work\design_libs/shared_lib/fifo_sync/pkg.conf'

   -- $Revision: 1.4 $
   
   type rConf_Shared_lib_Fifo_sync is
      record
         data_width     : integer ;
         numwords       : integer;
         reset_polarity : integer;
         numwords_width : integer;
         techno         : integer;   
         cSetArch       : integer; -- architecture selection for socket.vhd
      end record;

   constant  cConf_Shared_lib_Fifo_sync : rConf_Shared_lib_Fifo_sync  :=
      (
         data_width     =>  8,
         numwords       =>  256,
         reset_polarity =>  0,
         numwords_width =>  8,
         techno         =>  1, -- select RAM techno (1=generic implementation)
         cSetArch       =>  1 -- default is generic implementation
      );
      
   
-- shared_lib.ram_sync_dp imported from 'd:\user\projet\hdl\renoir\work\design_libs/shared_lib/ram_sync_dp/pkg.conf'
   -- $Revision: 1.3 $
   
   type rConf_Shared_lib_Ram_sync_dp is -- type declaration
      record -- here start the record content dump
         DataSize : integer ; -- RAM data size
         AddrSize : integer;  -- RAM address size
         techno   : integer;  -- this pointer select target technology between multiple architecture
      
      end record;
   constant  cConf_Shared_lib_Ram_sync_dp: rConf_Shared_lib_Ram_sync_dp  := -- initial value declaration
      ( -- here start default values
         DataSize => 8,
         AddrSize => 9,
         techno   => 2 -- default is SPARTAN2
      );
      
-- shared_lib.ram_sync_sp imported from 'd:\user\projet\hdl\renoir\work\design_libs/shared_lib/ram_sync_sp/pkg.conf'
   
   -- $Revision: 1.1 $
   
   constant cShared_lib_Ram_sync_sp_Techno_Altera : integer :=0;
   
   type rConf_Shared_lib_Ram_sync_sp is
      record
         DataSize : integer ;
         AddrSize : integer;
         techno   : integer;   
      
      end record;
   constant  cConf_Shared_lib_Ram_sync_sp: rConf_Shared_lib_Ram_sync_sp  :=
      (
         DataSize => 8,
         AddrSize => 6,
         techno   => 3 -- default is SPARTAN2
      );
      

      
END pkg_defines;
PACKAGE BODY pkg_defines IS
END pkg_defines;

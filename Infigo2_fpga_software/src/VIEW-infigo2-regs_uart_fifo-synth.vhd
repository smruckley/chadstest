
-- hds interface_end
architecture synth of regs_uart_fifo is

	-- Registers (CLK_IMX generated)
	signal sREG_IER			: std_logic_vector (3 downto 0);	-- Interrupt enable register
	signal sREG_IIR			: std_logic_vector (7 downto 0);	-- Interrupt identification register
	signal sREG_FCR			: std_logic_vector (7 downto 0);	-- FIFO control register
	signal sREG_LCR			: std_logic_vector (7 downto 0);	-- Line control register
	signal sREG_MCR			: std_logic_vector (7 downto 0);	-- Modem control register
	signal sREG_LSR			: std_logic_vector (7 downto 0);	-- Line status register
	signal sREG_MSR			: std_logic_vector (7 downto 0);	-- Modem status register
	signal sREG_SCR			: std_logic_vector (7 downto 0);	-- Scratch register
	signal sREG_DLL			: std_logic_vector (7 downto 0);	-- Divisor latch register LSB
	signal sREG_DLM			: std_logic_vector (7 downto 0);	-- Divisor latch register MSB
	
	signal sData_RX			: std_logic_vector (7 downto 0);
	
	signal sCNT				: unsigned (15 downto 0);			-- Baud rate tick counter
	signal sTriggerLevel	: unsigned (gNumwords_width - 1 downto 0);
	signal sAddr			: std_logic_vector (3 downto 0);	-- Internal address
	signal sDLAB			: std_logic;						-- Bank select register

    signal sHard_flow       : std_logic;
    signal sWrite_fifo_tx	: std_logic;
	signal sWr_fifo_rx_ld	: std_logic;
	signal sWrite_fifo_rx	: std_logic;
    signal sRead_fifo_rx	: std_logic;
	signal sReset_fifo_rx	: std_logic;
	
	signal sModstat_int		: std_logic;
	signal sBreakdetected	: std_logic;
	signal sParitydetected	: std_logic;
	signal sFramedetected	: std_logic;
	signal sTimeout			: std_logic;
	signal CTS_1			: std_logic;
	signal sTrgReached		: std_logic;
	signal snRTS			: std_logic;
	signal sCTS				: std_logic;
	signal sDSR				: std_logic;
	signal sRI				: std_logic;
	signal sDCD				: std_logic;
	
	constant cZero			: std_logic_vector(gNumwords_width - 1 downto 0) := (others => '0');
	constant cOne			: unsigned(gNumwords_width - 1 downto 0) := to_unsigned(1, gNumwords_width);
	constant cTwo			: unsigned(gNumwords_width - 1 downto 0) := to_unsigned(gFifoLvl1, gNumwords_width);
	constant cThree			: unsigned(gNumwords_width - 1 downto 0) := to_unsigned(gNumwords / 2, gNumwords_width);
	constant cFour			: unsigned(gNumwords_width - 1 downto 0) := to_unsigned(gNumwords-(gFifoLvl1/2), gNumwords_width);
	constant cFull			: std_logic_vector(gNumwords_width - 1 downto 0) := std_logic_vector(to_unsigned(gNumwords-1, gNumwords_width));
	
begin

	sDLAB    <= sREG_LCR(7);
	divider  <= "1111";
	sAddr    <= sDLAB & Addr;
	TxD      <= TX_in and (not sREG_LCR(6));		-- Bit 6 of LCR used to send break... Active high
	sData_RX <= DataRX(sData_RX'RANGE) when fifo_rx_empty = '0' and sReset_fifo_rx = '0' else (others => '0');
	
	with sAddr select	-- Uses DLAB + 3 bits address bus
	DataOut <= sData_RX           	when "0000",		-- Access DATA register, read mode    (Bank 0)
	           "0000" & sREG_IER    when "0001",		-- Access IER register 		          (Bank 0)
	           sREG_IIR		       	when "0010",		-- Access IIR register             	  (Bank 0)
	           sREG_LCR		       	when "0011",		-- Access LCR register                (Bank 0)
	           sREG_MCR				when "0100",		-- Access MCR register                (Bank 0)
	           sREG_LSR 	        when "0101",		-- Access LSR register                (Bank 0)
	           sREG_MSR				when "0110",		-- Access MSR register                (Bank 0)
	           sREG_SCR	        	when "0111",		-- Access SCR register                (Bank 0)
			   sREG_DLL           	when "1000",		-- Access DLL register			      (Bank 1)
	           sREG_DLM  	        when "1001",		-- Access DLM register 		          (Bank 1)
	           sREG_IIR		       	when "1010",		-- Access IIR register             	  (Bank 1)
	           sREG_LCR		       	when "1011",		-- Access LCR register                (Bank 1)
	           sREG_MCR				when "1100",		-- Access MCR register                (Bank 1)
	           sREG_LSR 	        when "1101",		-- Access LSR register                (Bank 1)
	           sREG_MSR				when "1110",		-- Access MSR register                (Bank 1)
	           sREG_SCR	        	when "1111",		-- Access SCR register                (Bank 1)
			   (others => '0')  	when others;

	N8bits            <= sREG_LCR(1) and sREG_LCR(0);		-- '10' : 7 bits words, '11' : 8 bits word
	ParityEn          <= sREG_LCR(3);						-- Parity enabled
	EvenPar           <= sREG_LCR(4);						-- Parity selection even/odd
	NbStop            <= sREG_LCR(2);						-- '0' : 1 stop bit, '1' : 2 stop bits
	sHard_flow        <= sREG_MCR(5);						-- Automatic hardware flow control selection
	hard_flow         <= sHard_flow;
	write_fifo_tx     <= sWrite_fifo_tx;
	read_fifo_rx      <= sRead_fifo_rx;
	GetNextData       <= sRead_fifo_rx;
	fifo_rx_empty_lat <= fifo_rx_empty;
	TimeOutDur        <= X"2C";			        -- 4*11 bits expressed in baud rate ticks number
--	sBreakDetected    <= BreakDetected when fifo_rx_empty = '0' and sReset_fifo_rx = '0' else '0';
	sREG_LSR(0)       <= not fifo_rx_empty;
	sREG_LSR(1)       <= fifo_rx_full;
	sREG_LSR(2)       <= DataRX(8)  when fifo_rx_empty = '0' and sReset_fifo_rx = '0' else '0';
	sREG_LSR(3)       <= DataRX(9)  when fifo_rx_empty = '0' and sReset_fifo_rx = '0' else '0';
	sREG_LSR(4)       <= DataRX(10) when fifo_rx_empty = '0' and sReset_fifo_rx = '0' else '0';
	sREG_LSR(5)       <= fifo_tx_empty;
	sREG_LSR(6)       <= fifo_tx_empty and not TxBusy;
	sREG_LSR(7)       <= '0';
	nDTR              <= not sREG_MCR(0);
	nRTS              <= snRTS when sREG_MCR(5) = '1' else not sREG_MCR(1);
	sREG_MSR(4)       <= not nCTS;
	sREG_MSR(5)       <= not nDSR;
	sREG_MSR(6)       <= not nRI;
	sREG_MSR(7)       <= not nDCD;
	sReset_fifo_rx    <= sREG_FCR(1) or not nRESET;
	reset_fifo_rx     <= sReset_fifo_rx;
	reset_fifo_tx     <= sREG_FCR(2) or not nRESET;
	sREG_IIR(7)       <= '1';
	sREG_IIR(6)       <= '1';
	sREG_IIR(5)       <= '0';
	sREG_IIR(4)       <= '0';
	nIRQ              <= sREG_IIR(0);

	                    
	pDECOD:	process (CLK, nRESET)
	begin
	
		if nRESET = '0' then
			sWrite_fifo_tx        <= '0';
			sRead_fifo_rx         <= '0';
			sWr_fifo_rx_ld        <= '0';
			sWrite_fifo_rx        <= '0';
			write_fifo_rx         <= '0';
			sTrgReached           <= '0';
			sModstat_int          <= '0';
			sTimeout              <= '0';
			sBreakdetected        <= '0';
			sParitydetected       <= '0';
			sFramedetected        <= '0';
			sCTS                  <= '1';
			sDSR                  <= '1';
			sRI                   <= '1';
			sDCD                  <= '1';
			DataTX                <= (others => '0');
			sREG_IER              <= (others => '0');
			sREG_FCR              <= (others => '0');
			sREG_LCR              <= (others => '0');
			sREG_MCR              <= (others => '0');
			sTriggerLevel         <= (others => '0');
			sREG_MSR (3 downto 0) <= (others => '0');
			sREG_SCR              <= (others => '0');
			sREG_DLL              <= (others => '0');
			sREG_DLM              <= (others => '0');
			sREG_IIR (3 downto 0) <= "0001";
		elsif Rising_Edge (CLK) then
			-- Default signal states (ticks)
			sRead_fifo_rx     <= '0';
			sWrite_fifo_tx    <= '0';
			write_fifo_rx     <= '0';
			sCTS              <= nCTS;
			sDSR              <= nDSR;
			sRI               <= nRI;
			sDCD              <= nDCD;
			sWr_fifo_rx_ld    <= DataReceived;
			sWrite_fifo_rx    <= sWr_fifo_rx_ld;
			if sWr_fifo_rx_ld = '1' and sWrite_fifo_rx = '0' then	-- Rising edge on DataReceived
				write_fifo_rx <= '1';								-- Generate one tick
			end if;
			if TimeOut = '1' then
				sTimeout <= '1';		-- Latch timeout signal
			end if;
			if sCTS /= nCTS then		-- Change in CTS
				sREG_MSR(0)  <= '1';
				sModstat_int <= '1';
			end if;
			if sDSR /= nDSR then		-- Change in DSR
				sREG_MSR(1)  <= '1';
				sModstat_int <= '1';
			end if;
			if sRI = '0' and nRI = '1' then	-- Trailing RI signal
				sREG_MSR(2)  <= '1';
				sModstat_int <= '1';
			end if;
			if sDCD /= nDCD then		-- Change in DCD
				sREG_MSR(3)  <= '1';
				sModstat_int <= '1';
			end if;
			if sREG_IER(2) = '1' and (sParitydetected = '1' or sFramedetected = '1' or sBreakdetected = '1' or fifo_rx_full = '1') then	-- Receiver line status interrupt
				sREG_IIR(3 downto 0) <= "0110";
			elsif sREG_IER(0) = '1' and sTrgReached = '1' then
				sREG_IIR(3 downto 0) <= "0100";
			elsif sREG_IER(0) = '1' and (NumWordsReceived /= cZero and sTimeout = '1') then
				sREG_IIR(3 downto 0) <= "1100";
			elsif sREG_IER(1) = '1' and sREG_LSR(5) = '1' then
				sREG_IIR(3 downto 0) <= "0010";
			elsif sREG_IER(3) = '1' and sModstat_int = '1' then
				sREG_IIR(3 downto 0) <= "0000";
			else sREG_IIR(3 downto 0) <= "0001";
			end if;
			if (unsigned(NumWordsReceived) >= sTriggerLevel) then
				sTrgReached <= '1';
			else
				sTrgReached <= '0';
			end if;
			if ReceivedBreak = '1' then
				sBreakdetected <= '1';	-- Latch break signal
			end if;
			if ReceivedParityErr = '1' then
				sParitydetected <= '1';	-- Latch parity error signal
			end if;
			if ReceivedFrameErr = '1' then
				sFramedetected <= '1';	-- Latch frame error signal
			end if;
			if nCS = '0' and nWrite = '0' then		-- WRITE REGISTERS
				case AD_SYNC is
					when "000" =>
						if sDLAB = '0' then
							sWrite_fifo_tx <= '1';		-- Write data into TX FIFO
							DataTX         <= DataIn;	-- Store data to send
						else
							sREG_DLL       <= Datain;
						end if;
					when "001" =>
						if sDLAB = '0' then
							sREG_IER       <= DataIn(3 downto 0);
						else
							sREG_DLM       <= Datain;
						end if;
					when "010" =>
						sREG_FCR       <= Datain;
						case Datain(7 downto 6) is
							when "00" =>
								sTriggerLevel <= cOne;
							when "01" =>
								sTriggerLevel <= cTwo;
							when "10" =>
								sTriggerLevel <= cThree;
							when "11" =>
								sTriggerLevel <= cFour;
							when others =>
								sTriggerLevel <= cOne;
						end case;
					when "011" =>
						sREG_LCR       <= DataIn;
					when "100" =>
						sREG_MCR       <= Datain;
					when "110" =>
						sREG_MSR(3 downto 0) <= Datain (3 downto 0);	-- Write to modem status flags
						sModstat_int         <= '0';
					when "111" =>
						sREG_SCR       <= Datain;
					when others =>
						NULL;
				end case;
			elsif nCS = '0' and nRead = '0' then	-- Action after a read access
				case AD_SYNC is
					when "000" =>					-- Reading THR register will read FIFO
						if sDLAB = '0' then
							sRead_fifo_rx <= '1';
							sTimeout      <= '0';		-- Reset timeout condition
						end if;
					when "101" =>
						sBreakdetected  <= '0';
						sParitydetected <= '0';
						sFramedetected  <= '0';
					when "110" =>
						sREG_MSR(3 downto 0) <= (others => '0');
						sModstat_int         <= '0';
					when others =>
						NULL;
				end case;
			end if;
		end if;
	end process;

	
pCLOCK:	process (CLK_COM, nRESET)
	begin
		if nRESET = '0' then
			sCNT <= (others => '0');
			Tick <= '0';
		elsif Rising_Edge (CLK_COM) then
			Tick <= '0';		-- default signal
			if sCNT < unsigned(sREG_DLM & sREG_DLL)-1 then
				sCNT <= sCNT + 1;
			else
				sCNT <= (others => '0');
				Tick <= '1';
			end if;
		end if;
	end process;
	
pFUNC: process (CLK_COM, nRESET)
	begin
		if nRESET = '0' then
			CTS_1   <= '0';
			CTS_reg <= '0';
			snRTS   <= '1';
		elsif Rising_Edge (CLK_COM) then
			CTS_1   <= not nCTS;
			CTS_reg <= CTS_1;
			if sREG_MCR(5) = '1' then
				case sREG_FCR(7 downto 6) is
					when "11" =>
						if NumWordsReceived = cFULL then	-- Only one byte remaining in FIFO
							snRTS <= '1';					-- Deassert RTS
						else
							snRTS <= '0';					-- Reassert RTS
						end if;
					when others =>
						if sTrgReached = '1' then			-- FIFO trigger level reached
							snRTS <= '1';					-- Deassert RTS
						elsif fifo_rx_empty = '1' then			-- FIFO empty
							snRTS <= '0';					-- Reassert RTS
						end if;
				end case;
			end if;
		end if;
	end process;
end synth;


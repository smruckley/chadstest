
-- hds interface_end

-- hds library vlo machxo2


architecture Structure of powctrl is

    -- internal signal declarations
    signal scuba_vlo: std_logic;

    -- local component declarations
    component VLO
        port (Z: out  std_logic);
    end component;
    component PCNTR
        generic (BGOFF : in String; POROFF : in String; 
                WAKEUP : in String; TIMEOUT : in String; 
                STDBYOPT : in String);
        port (CLK: in  std_logic; USERTIMEOUT: in  std_logic; 
            USERSTDBY: in  std_logic; CLRFLAG: in  std_logic; 
            CFGSTDBY: in  std_logic; CFGWAKE: in  std_logic; 
            STOP: out  std_logic; STDBY: out  std_logic; 
            SFLAG: out  std_logic);
    end component;

begin
    -- component instantiation statements
    scuba_vlo_inst: VLO
        port map (Z=>scuba_vlo);

    PCNTR_Inst0: PCNTR
        generic map (BGOFF=> "TRUE", POROFF=> "TRUE", WAKEUP=> "USER_CFG", 
        TIMEOUT=> "COUNTER", STDBYOPT=> "USER_CFG")
        port map (CLK=>CLK, USERTIMEOUT=>scuba_vlo, USERSTDBY=>USERSTDBY, 
            CLRFLAG=>CLRFLAG, CFGSTDBY=>CFGSTDBY, CFGWAKE=>CFGWAKE, 
            STOP=>STOP, STDBY=>STDBY, SFLAG=>SFLAG);

end Structure;


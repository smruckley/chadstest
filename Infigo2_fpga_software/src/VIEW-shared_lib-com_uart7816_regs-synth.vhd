
-- hds interface_end

-- **********************************************************************************
-- RCS revision  : $Revision: 1.1 $
-- RCS history   : $Log: synth.vhd,v $
-- RCS history   : Revision 1.1  2004/09/08 13:08:59  gehin
-- RCS history   : initial revision
-- RCS history   :
-- **********************************************************************************

-------------------------------------------------------------------------------------
-- put here user comments :

architecture synth of com_uart7816_regs is

   -- registers address
   constant cREG_DATA         : std_logic_vector(2 downto 0) := "000";
   constant cREG_CONF         : std_logic_vector(2 downto 0) := "001";
   constant cREG_STAT         : std_logic_vector(2 downto 0) := "010";
   constant cREG_MASK         : std_logic_vector(2 downto 0) := "011";
   constant cREG_CARD         : std_logic_vector(2 downto 0) := "100";
   constant cREG_DIV1         : std_logic_vector(2 downto 0) := "101";
   constant cREG_DIV2         : std_logic_vector(2 downto 0) := "110";
   constant cREG_DIV3         : std_logic_vector(2 downto 0) := "111";
    
	signal sREG_CONF 		      : std_logic_vector (7 downto 0);  -- Status byte
	signal sREG_STAT    		   : std_logic_vector (7 downto 0);  -- Primary configuration register (bank 0)
	signal sREG_MASK    		   : std_logic_vector (7 downto 0);  -- Secondary configuration register (bank 1)
	signal sREG_CARD 		      : std_logic_vector (7 downto 0);  -- CARD control register
--	signal sREG_ACK 		      : std_logic_vector (7 downto 0);  -- INS register for procedure byte comparison

   constant   cTransmit_mode   : std_logic := '0';
   
   signal sMode               : std_logic;
   signal sExtTrigger0        : std_logic;
   signal sExtTrigger1        : std_logic;
--   signal sExtTrigger_event   : std_logic;
   signal sEnable_ExtTrigger  : std_logic;
   

   signal sRXTX_IO            : std_logic;
--	signal sREG_DIV1 		      : std_logic_vector (7 downto 0);  -- Interrupt mask
	signal sRX_IN              : std_logic;
	

--	signal sREG_DIV2 		   : std_logic_vector (7 downto 0);  -- Interrupt mask
--	signal sREG_DIV3 		   : std_logic_vector (7 downto 0);  -- Interrupt mask

	signal sCntData 		   : unsigned (7 downto 0);
   signal sCntDataDec      : std_logic;
   signal sTX_OUT          : std_logic;
   signal   sTxBusy        : std_logic;
   
   signal sETU_compens,sEnable_clock_icc,sSelect_ext_clock_icc    : std_logic;
   signal sICC_DIV                                                : std_logic_vector(ICC_DIV'RANGE);
   
   signal sHSP_MODE        : std_logic;
   
   
begin

   n8bits                  <= '1'   ; -- always 8 bits data transfer
   nbstop                  <= '0'   ; -- not used
   --even_parity             <= '1'   ; -- always odd parity
   indirect_iso_conv       <= sREG_CONF(2);
   parity_enable           <= sREG_CONF(3);
   char_repeat             <= sREG_CONF(4);
   even_parity             <= not sREG_CONF(5);
   
   sREG_STAT(1)            <= fifo_full;
   sREG_STAT(2)            <= fifo_empty;
   sREG_STAT(4)            <= not TxBusy;
   sREG_STAT(5)            <= guard_elapsed;
   sREG_STAT(6)            <= wait_elapsed;

   sMode                   <= sREG_CARD(0); -- '0' = transmitter, '1' = receiver
   fifo_reset              <= sREG_CARD(1); -- active low
   UART_reset              <= sREG_CARD(2); -- active low
   sEnable_ExtTrigger      <= sREG_CARD(3); -- '1' = external trigger enabled 
   counters_enable         <= sREG_CARD(4); -- '1' = enable wait and guard time counters
   counters_clock_source   <= sREG_CARD(5); -- '0' = select half ETU tick else select half ICC tick clock
   sRXTX_IO                <= sREG_CARD(7); -- drives TX_OUT pin 
   
   dataTX      <= data_fifo_out; -- set data to send for transmitter component
   
   ICC_DIV                 <= sICC_DIV;
   ETU_compens             <= sETU_compens;
   Enable_clock_icc        <= sEnable_clock_icc;
   select_ext_clock_icc    <= sSelect_ext_clock_icc;
   
   -- tri-state RX/TX output

gTRISTATE:   
   if enable_tristate=1 generate
	   RX_TX       <= '0' when sTX_OUT='0' else 'Z';                 -- else line is released (tri-stated)
	end generate;

   -- for simulation only
   -- pragma synthesis_off
gSIMUL:
   if enable_tristate=0 generate
	   RX_TX       <= 'Z';
	end generate;
   -- pragma synthesis_on

--   PROCBYTE <= sREG_ACK;
   PROCBYTE <= (others=>'0');
   
   TX_OUT <= sTX_OUT;
   sTX_OUT <=      TX_receiver      -- receiver action (used to indicate a parity error)
	            and TX_transmitter   -- normal transmitter action 
	            and sRXTX_IO;        -- user action
   
	with Addr select -- registers read access
	DataOut <=  data_fifo_out        when cREG_DATA,
	            sREG_CONF 		      when cREG_CONF,  
	       		sREG_STAT            when cREG_STAT,	
	       		sREG_MASK 		      when cREG_MASK,	
	       		sREG_CARD            when cREG_CARD, 
	       		sRX_IN & sSelect_ext_clock_icc & sEnable_clock_icc & sETU_compens & sICC_DIV  when others;
   
   -- interrupt generation (active high)
	IRQ <= '1' when ((sREG_STAT(0) = '1' and sREG_MASK(0) = '1') or -- STAT interrupts
	                 (sREG_STAT(1) = '1' and sREG_MASK(1) = '1') or
	                 (sREG_STAT(2) = '1' and sREG_MASK(2) = '1') or
	                 (sREG_STAT(3) = '1' and sREG_MASK(3) = '1') or
	                 (sREG_STAT(4) = '1' and sREG_MASK(4) = '1') or
	                 (sREG_STAT(5) = '1' and sREG_MASK(5) = '1') or
	                 (sREG_STAT(6) = '1' and sREG_MASK(6) = '1') or
	                 (sREG_STAT(7) = '1' and sREG_MASK(7) = '1') or
	                 (sREG_CONF(6) = '1' and sREG_CONF(7) = '1')
                    )
	                 
	          else '0';

----------------------------------------------------------------------------
-- decod register address and manage data bus
----------------------------------------------------------------------------
   
   pDECOD: process (CLK,nRST) 
   begin
		  
      if nRST='0' then

         read_fifo   <= '0';
      	write_fifo  <= '0';
 		   
-- 		   dataTX         <= (others=>'0');
 		   data_fifo_in   <= (others=>'0');
 		   
 		   HETU_DIV    <= (others=>'0');
 		   GUARD_VALUE <= (others=>'0');
 		   WAIT_VALUE  <= (others=>'0');
 		   
 		   sREG_CONF(7 downto 0)      <= (others=>'0');
 		   sREG_STAT(0)   <= '0';
 		   sREG_STAT(3)   <= '0';
 		   sREG_STAT(7)   <= '0';
-- 		   sREG_DIV1      <= "10000000"; -- keep I/O line to '1'
 		   
 		   sREG_MASK            <= (others=>'0');
 		   sREG_CARD            <= "00000000"; -- keep ICC I/O line and ICC RESET line to '0'
-- 		   sREG_ACK             <= (others=>'0');

 		   sICC_DIV                <= (others=>'0');
 		   sETU_compens            <= '0';
 		   sEnable_clock_icc       <= '0'; -- set ICC clock to '0'
         sSelect_ext_clock_icc   <= '1'; -- enable external ICC clock
         
 		   start_xmit           <= '0';
    	   counters_reload      <= '0';
--         sMode                <= '0';
         
--         sRXTX_IO             <= '1'; -- kepp line to 'Z'
--         RESET_ICC            <= '0';
--         UART_reset           <= '0';
         
 		   
         sExtTrigger0         <= '0';
         sExtTrigger1         <= '0';
--         sExtTrigger_event    <= '0';
--         sEnable_ExtTrigger   <= '0';
 		   
 		   sCntData             <= (others=>'0');
         sCntDataDec          <= '0';         
 		   sRX_IN               <= '1';
 		   
 		   sTxBusy              <= '0';
-- 		   sCntData_load        <= (others=>'0');
 		   enable_receiver      <='0';
 		   RESET_ICC            <= '0';
 		   sHSP_MODE            <= '0';
 		   
      elsif Rising_Edge(CLK) then

         if tick_CLKICC='1' then -- synchronize reset output on ICC clock
            RESET_ICC   <= sREG_CARD(6); -- drives card reset pin
         end if;

         enable_receiver         <= sMode and sREG_STAT(4); -- enable receiver only when transmission is done
      -- default assignments
      
         read_fifo   <= '0';
    	   write_fifo  <= '0'; 
    	   sRX_IN      <= RX_IN   ; -- metastability avoidance;
    	   
         
   		if (CS='1' and WRCYCLE = '1') then -- WRITE REGISTERS
   			case AD_SYNC is
   			   when cREG_DATA =>
   			      if sMode= cTransmit_mode then
   			         write_fifo <= '1';
   			         data_fifo_in <= DataBus;
   			      end if;
   			   when cREG_CONF =>
   			      sREG_CONF(7 downto 0) <= DataBus(7 downto 0);
   			   when cREG_STAT =>
          		   sREG_STAT(0)         <= DataBus(0);
          		   sREG_STAT(3)         <= DataBus(3);
          		   sREG_STAT(7)         <= DataBus(7);
   			   when cREG_MASK =>
   			      sREG_MASK <= DataBus;
   			   when cREG_CARD =>
   			      sREG_CARD <= DataBus;
               when cREG_DIV1 =>
                  case sREG_CONF(1 downto 0) is
--                     when "00"   =>
--                        sREG_ACK <= DataBus;
                     when "01"   =>
                        HETU_DIV (7 downto 0)   <= DataBus;
                     when "10"   =>
                        GUARD_VALUE(7 downto 0) <= DataBus;
                     when "11"   =>
                        WAIT_VALUE(7 downto 0)  <= DataBus;
                     when others =>
                        null;
                  end case;
               when cREG_DIV2 =>
                  case sREG_CONF(1 downto 0) is
                     when "00"   =>
--                        sCntData_load  <= unsigned(DataBus);
                        sCntData       <= unsigned(DataBus);
                     when "01"   =>
                        HETU_DIV (15 downto 8)  <= DataBus;
                     when "10"   =>
                        GUARD_VALUE(9 downto 8) <= DataBus(1 downto 0);
                     when "11"   =>
                        WAIT_VALUE(15 downto 8) <= DataBus;
                     when others =>
                        null;
                  end case;
               when cREG_DIV3 =>
                  case sREG_CONF(1 downto 0) is
                     when "01"   =>
                        sICC_DIV                <= DataBus(3 downto 0);
                        sETU_compens            <= Databus(4);
                        sEnable_clock_icc       <= DataBus(5);
                        sSelect_ext_clock_icc   <= DataBus(6);
                     when "10"   =>
                        sHSP_MODE               <= Databus(0);
                     when "11"   =>
                        WAIT_VALUE(23 downto 16) <= DataBus;
                     when others =>
                        null;
                  end case;
   				when others =>	
   					null;
   			end case;
   	   elsif (CS='1' and RDCYCLE = '1') then -- action after a read access
   			case AD_SYNC is
   				when cREG_DATA =>	-- reading THR register will request a FIFO read but **only** in receiver mode
         	      if sMode = not cTransmit_mode then
   				      read_fifo <= '1';
   				   end if;
   				when others =>	
   					null;
   			end case;
   		end if;

-- status flags

         sCntDataDec <= '0';         

         sTxBusy <= TxBusy;
         if TxBusy='0' and sTxBusy='1' then -- detect falling edge = end of transmission
            if tx_parity_error ='1' then
      			sREG_STAT(3)   <=  '1'; -- latch parity error status
            end if;
         end if;
         
         -- character received
         if DataReceived='1' then      
   			sREG_STAT(0)   <= '1';              -- set data ready flag
            if rx_parity_error ='1' then
      			sREG_STAT(3)   <=  '1'; -- latch parity error status
            end if;
--            if sEnable_ExtTrigger='0' then
--         	   sREG_STAT(7) <= SW1_byte_detected;
--            end if;
   	      if sMode = not cTransmit_mode then -- store data into FIFO if reception mode
               -- if HSP mode selected then store received byte in data counter
               -- but only one time (should be for first received byte, as first byte is frame size in HSP protocol)
   	         if sHSP_MODE='1' then
   	            sHSP_MODE <= '0'; -- disable HSP mode for one-shot action
   	            sCntData <= unsigned(DataRX);
   	         end if;
   	         sCntDataDec <= '1'; -- request data counter decrementation
   	         data_fifo_in <= DataRX;
   	         write_fifo  <= '1';
   	      end if;
         end if;

         -- emission requested
 		   start_xmit  <= '0';
         if start_transmit='1' and sMode= cTransmit_mode then
 	         sCntDataDec <= '1';  -- request data counter decrementation
            start_xmit  <= '1';  -- request emission
            read_fifo   <= '1';  -- get next data
         end if;

         -- manage data counter
         sREG_CONF(6) <='0'; -- default value
         if sCntData=0 then
            sREG_CONF(6)   <='1';
         elsif sCntDataDec='1' then -- decrement data counter 
            sCntData <= sCntData -1;
         end if;
      	
      	-- if rising edge on external trigger input then restart waiting time and guard time counter.
      	-- Triggering must also be enabled (sEnable_ExtTrigger signal)
    	   counters_reload <= '0';

         -- counters_ext_trigger is sampled each ICC clock cycle
      	if tick_CLKICC='1' then
         	sExtTrigger0 <= counters_ext_trigger;
         	sExtTrigger1 <= sExtTrigger0;
       	   if sExtTrigger1='0' and sExtTrigger0='1' and sEnable_ExtTrigger='1' then 
         	   counters_reload <='1';  -- reload wait time and guard time counters
         	   sREG_STAT(7) <= '1';
            end if;
    	   end if;
   
      	if rx_startbit='1' or tx_startbit='1' then
      	   sREG_CARD(5) <= '0';    -- automatically select ETU clock for counters
      	   counters_reload <='1';  -- reload wait time and guard time counters
      	end if;

   	end if;


   end process pDECOD;

end synth;




-- hds interface_end
architecture synth of com_uart7816_counter is
   signal sCnt : unsigned(LOAD_VALUE'range);
begin
   process (nRST,CLK)
      begin

         if nRST='0' then
            sCnt        <= (others=>'0');
            count_done  <='0';
         elsif rising_edge(CLK) then
         
            if reload='0' and enable='1' then
               if sCnt = 0 then
                  count_done <= '1';
               elsif (clk_sel='0' and CLK_CNT1='1') or (clk_sel='1' and CLK_CNT2='1') then
                  sCnt <= sCnt-1 ;
               end if;
            else
               sCnt <= unsigned(LOAD_VALUE);
               count_done  <='0';
            end if;
            
--            if reload='1' then
--               sCnt <= unsigned(load_value);
--            end if;

         end if;

      end process;
end synth;


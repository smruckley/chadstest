
-- renoir interface_end

-- **********************************************************************************
-- RCS revision  : $Revision: $
-- RCS history   : $Log$
-- **********************************************************************************
-- Describe below component summary:
-- Sections:
--    class    : enter device class (family) , example : communication, interrupt controller, pio, specific... 
--    top      : must be yes for a top component, else no
--    device   : summarize device functionnality, example : UART with RX/TX FIFO and flow control ...
--    status   : enter developpment status, examples : stable, developpment, experimental ...
-- 
-- @begin
-- [class]
-- COMMUNICATION DEVICE
-- [top]
-- NO
-- [device]
-- SC28L92 compatible DUART
-- [usage]
-- PLATFORM VALIDATOR
-- [status]
-- UNDER DEVELOPMENT
-- @end
-- 
-- --------------------------------------------------------------------
-- -- * file revision: $Revision: 1.1 $
-- -- * file history : $Log: struct.bd,v $
-- -- * file history : Revision 1.1  2012/06/26 14:08:19  pschirrer
-- -- * file history : Initial version
-- -- * file history :
-- -- * file history : Revision 1.5  2010/06/30 14:19:29  pschirrer
-- -- * file history : Temporary Fixed FIFO reset problem and FIFO output when empty
-- -- * file history :
-- -- * file history : Revision 1.4  2010/01/31 12:15:36  pschirrer
-- -- * file history : Added Fifo TX FULL status
-- -- * file history :
-- -- * file history : Revision 1.3  2009/11/23 08:56:07  pschirrer
-- -- * file history : Minor changes
-- -- * file history :
-- -- * file history : Revision 1.2  2009/11/11 14:11:05  pschirrer
-- -- * file history : First working test
-- -- * file history :
-- -- * file history : Revision 1.1  2009/11/05 11:51:20  pschirrer
-- -- * file history : Initial version
-- -- * file history :
-- --
-- -- User comments:
-- --
-- --------------------------------------------------------------------
-- renoir header_start
-- **********************************************************************************
--  Block Diagram generated by Mentor Graphics' Renoir(TM) 2002.1b (Build 7)
--   Library: infigo2
--  Copyright Schlumberger Systems, 2000-2001
-- **********************************************************************************
-- renoir header_end
-- Generation properties:
--   Component declarations : yes
--   Configurations         : embedded statements
--                          : include view name
--   
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
library shared_lib;
use shared_lib.pkg_defines.ALL;

library infigo2;
library shared_lib;

architecture struct of uart_fifo_modem is

   -- Architecture declarations

   -- Internal signal declarations
   signal Break             : std_logic;
   signal CTS_reg           : std_logic;
   signal CntNumWords       : std_logic_vector(gConf.numwords_width - 1 downto 0);
   signal DataFifoRX        : std_logic_vector(10 downto 0);
   signal DataFifoTX        : std_logic_vector(7 downto 0);
   signal DataRX            : std_logic_vector(7 downto 0);
   signal DataReceived      : std_logic;
   signal DataTX            : std_logic_vector(7 downto 0);
   signal EvenPar           : std_logic;
   signal FrameErr          : std_logic;
   signal GetNextData       : std_logic;
   signal LineFree          : std_logic;
   signal N8Bits            : std_logic;
   signal NbStop            : std_logic;
   signal ParityEn          : std_logic;
   signal ParityErr         : std_logic;
   signal RecBus            : std_logic_vector(10 downto 0);
   signal StartDetected     : std_logic;
   signal TX                : std_logic;
   signal Tick              : std_logic;
   signal TimeOut           : std_logic;
   signal TimeOutDur        : std_logic_vector(7 downto 0);
   signal TxBusy            : std_logic;
   signal TxEmpty           : std_logic;
   signal divider           : std_logic_vector(3 downto 0);
   signal fifo_rx_empty     : std_logic;
   signal fifo_rx_empty_lat : std_logic;
   signal fifo_rx_full      : std_logic;
   signal fifo_tx_empty     : std_logic;
   signal fifo_tx_full      : std_logic;
   signal hard_flow         : std_logic;
   signal rd_fifo_tx        : std_logic;
   signal read_fifo_rx      : std_logic;
   signal reset_fifo_rx     : std_logic;
   signal reset_fifo_tx     : std_logic;
   signal start_xmit        : std_logic;
   signal write_fifo_tx     : std_logic;
   signal write_fifo_rx     : std_logic;


   -- Component Declarations
   component com_uart_rx_div
   generic (
      gConf : rConf_Shared_lib_Com_uart_rx_div := cConf_Shared_lib_Com_uart_rx_div
   );
   port (
      EvenPar       : in     std_logic ;                               -- Even parity select
      CLK           : in     std_logic ;                               -- System Clock
      ParityEn      : in     std_logic ;                               -- Parity select
      Rx            : in     std_logic ;                               -- Serial data in
      NbStop        : in     std_logic ;                               -- Parity stick bit
      Tick          : in     std_logic ;                               -- Baud x 16 tick
      nRST          : in     std_logic ;                               -- asynchronous reset active low
      Dout          : out    std_logic_vector (gConf.NDbits-1 downto 0);
      FrameErr      : out    std_logic ;                               -- Framing error detected
      ParityErr     : out    std_logic ;                               -- Parity error detected
      DataReceived  : out    std_logic ;
      -- Status flag updated indicator
      -- Status signals valid
      divider       : in     std_logic_vector (3 downto 0);
      Break         : out    std_logic ;
      -- Status flag updated indicator
      -- Status signals valid
      StartDetected : out    std_logic ;
      -- Status flag updated indicator
      -- Status signals valid
      N8Bits        : in     std_logic                                 -- Parity select
   );
   end component;
   component com_uart_timeout
   port (
      CLK_COM           : in     std_logic ;
      DataReceived      : in     std_logic ;
      GetNextData       : in     std_logic ;
      StartDetected     : in     std_logic ;
      Tick              : in     std_logic ;
      TimeOutDur        : in     std_logic_vector (7 downto 0);
      divider           : in     std_logic ;
      fifo_rx_empty_lat : in     std_logic ;
      nRESET            : in     std_logic ;
      LineFree          : out    std_logic ;
      TimeOut           : out    std_logic 
   );
   end component;
   component com_uart_tx_div
   generic (
      NDbits : positive := 8
   );
   port (
      CLK      : in     std_logic ;                           -- System Clock
      Din      : in     std_logic_vector ((NDbits-1) downto 0);
      EvenPar  : in     std_logic ;                           -- Even parity select
      LD       : in     std_logic ;                           -- Load transmit register
      ParityEn : in     std_logic ;                           -- Parity select
      NbStop   : in     std_logic ;                           -- Parity stick bit
      Tick     : in     std_logic ;                           -- Baud x 16 tick
      nRST     : in     std_logic ;                           -- asynchronous reset active low
      Tx       : out    std_logic ;                           -- Serial data out
      TxBusy   : out    std_logic ;                           -- Transmitter busy
      TxEmpty  : out    std_logic ;                           -- Transmit buffer empty
      divider  : in     std_logic_vector (3 downto 0);
      N8Bits   : in     std_logic                             -- select 7 or 8 bits mode
   );
   end component;
   component fifo_sync
   generic (
      gConf : rConf_Shared_lib_Fifo_sync := cConf_Shared_lib_Fifo_sync
   );
   port (
      CLK         : in     std_logic ;
      data        : in     std_logic_vector (gConf.data_width-1 downto 0);
      rdreq       : in     std_logic ;
      reset       : in     std_logic ;
      wrreq       : in     std_logic ;
      CntNumWords : out    std_logic_vector (gConf.numwords_width downto 0);
      empty       : out    std_logic ;
      full        : out    std_logic ;
      q           : out    std_logic_vector (gConf.data_width-1 downto 0);
      readable    : out    std_logic ;
      writable    : out    std_logic 
   );
   end component;
   component regs_uart_fifo
   generic (
      gNumwords_width : positive := 10;
      gNumwords       : positive := 256;
      gFifoLvl1       : positive := 8
   );
   port (
      CLK               : in     std_logic ;
      nRESET            : in     std_logic ;
      nCS               : in     std_logic ;
      nRead             : in     std_logic ;
      nWrite            : in     std_logic ;
      Datain            : in     std_logic_vector (7 downto 0);
      Addr              : in     std_logic_vector (2 downto 0);
      TX_in             : in     std_logic ;
      TxBusy            : in     std_logic ;
      TxEmpty           : in     std_logic ;
      fifo_tx_empty     : in     std_logic ;
      fifo_rx_empty     : in     std_logic ;
      fifo_rx_full      : in     std_logic ;
      ReceivedParityErr : in     std_logic ;
      ReceivedFrameErr  : in     std_logic ;
      ReceivedBreak     : in     std_logic ;
      DataReceived      : in     std_logic ;
      TimeOut           : in     std_logic ;
      DataRX            : in     std_logic_vector (10 downto 0);
      DataOut           : out    std_logic_vector (7 downto 0);
      nIRQ              : out    std_logic ;
      TxD               : out    std_logic ;
      write_fifo_tx     : out    std_logic ;
      N8Bits            : out    std_logic ;
      ParityEn          : out    std_logic ;
      EvenPar           : out    std_logic ;
      NbStop            : out    std_logic ;
      divider           : out    std_logic_vector (3 downto 0);
      reset_fifo_rx     : out    std_logic ;
      reset_fifo_tx     : out    std_logic ;
      hard_flow         : out    std_logic ;
      Tick              : out    std_logic ;
      DataTX            : out    std_logic_vector (7 downto 0);
      TimeOutDur        : out    std_logic_vector (7 downto 0);
      nCTS              : in     std_logic ;
      nDCD              : in     std_logic ;
      nDSR              : in     std_logic ;
      nRI               : in     std_logic ;
      nDTR              : out    std_logic ;
      nRTS              : out    std_logic ;
      CTS_reg           : out    std_logic ;
      LineFree          : in     std_logic ;
      NumWordsReceived  : in     std_logic_vector (gNumwords_width - 1 downto 0);
      read_fifo_rx      : out    std_logic ;
      fifo_rx_empty_lat : out    std_logic ;
      fifo_tx_full      : in     std_logic ;
      CLK_COM           : in     std_logic ;
      GetNextData       : out    std_logic ;
      write_fifo_rx     : out    std_logic ;
      AD_SYNC           : in     std_logic_vector (2 downto 0)
   );
   end component;
   component xmit_seq
   port (
      CLK           : in     std_logic ;
      nRST          : in     std_logic ;
      hard_flow     : in     std_logic ;
      CTS_reg       : in     std_logic ;
      TxBusy        : in     std_logic ;
      fifo_tx_empty : in     std_logic ;
      rd_fifo_tx    : out    std_logic ;
      start_xmit    : out    std_logic 
   );
   end component;

   -- Optional embedded configurations
   for all : com_uart_rx_div use entity infigo2.com_uart_rx_div(fsm);
   for all : com_uart_timeout use entity infigo2.com_uart_timeout(synth);
   for all : com_uart_tx_div use entity shared_lib.com_uart_tx_div(fsm);
   for all : fifo_sync use entity infigo2.fifo_sync(struct);
   for all : regs_uart_fifo use entity infigo2.regs_uart_fifo(synth);
   for all : xmit_seq use entity infigo2.xmit_seq(fsm);


begin
   -- Architecture concurrent statements
   -- HDL Embedded Text Block 1 eb1
   -- eb1 1
   RecBus(7 downto 0) <= DataRX;
   RecBus(8)          <= ParityErr;
   RecBus(9)          <= FrameErr;
   RecBus(10)         <= Break;

   -- HDL Embedded Text Block 2 eb2
   -- eb2 2
   Baudout <= Tick;

   -- Instance port mappings.
   I_RX : com_uart_rx_div
      generic map (
         gConf => cConf_rx_div
      )
      port map (
         CLK           => CLK_COM,
         EvenPar       => EvenPar,
         ParityEn      => ParityEn,
         Rx            => RxD,
         NbStop        => NbStop,
         Tick          => Tick,
         nRST          => nRESET,
         Dout          => DataRX,
         FrameErr      => FrameErr,
         ParityErr     => ParityErr,
         DataReceived  => DataReceived,
         divider       => divider,
         Break         => Break,
         StartDetected => StartDetected,
         N8Bits        => N8Bits
      );
   LINE_CHECK : com_uart_timeout
      port map (
         CLK_COM           => CLK_COM,
         DataReceived      => DataReceived,
         GetNextData       => GetNextData,
         StartDetected     => StartDetected,
         Tick              => Tick,
         TimeOutDur        => TimeOutDur,
         divider           => divider(3),
         fifo_rx_empty_lat => fifo_rx_empty_lat,
         nRESET            => nRESET,
         LineFree          => LineFree,
         TimeOut           => TimeOut
      );
   I_TX : com_uart_tx_div
      generic map (
         NDbits => 8
      )
      port map (
         CLK      => CLK_COM,
         Din      => DataFifoTX,
         EvenPar  => EvenPar,
         LD       => start_xmit,
         ParityEn => ParityEn,
         NbStop   => NbStop,
         Tick     => Tick,
         nRST     => nRESET,
         Tx       => TX,
         TxBusy   => TxBusy,
         TxEmpty  => TxEmpty,
         divider  => divider,
         N8Bits   => N8Bits
      );
   TX_FIFO : fifo_sync
      generic map (
         gConf => cConf_TX_Fifo
      )
      port map (
         CLK         => CLK,
         data        => DataTX,
         rdreq       => rd_fifo_tx,
         reset       => reset_fifo_tx,
         wrreq       => write_fifo_tx,
         CntNumWords => open,
         empty       => fifo_tx_empty,
         full        => fifo_tx_full,
         q           => DataFifoTX,
         readable    => open,
         writable    => open
      );
   RX_FIFO : fifo_sync
      generic map (
         gConf => cConf_RX_Fifo
      )
      port map (
         CLK         => CLK,
         data        => RecBus,
         rdreq       => read_fifo_rx,
         reset       => reset_fifo_rx,
         wrreq       => write_fifo_rx,
         CntNumWords => CntNumWords,
         empty       => fifo_rx_empty,
         full        => fifo_rx_full,
         q           => DataFifoRX,
         readable    => open,
         writable    => open
      );
   REGS_UART : regs_uart_fifo
      generic map (
         gNumwords_width => gConf.numwords_width,
         gNumwords       => gConf.numwords,
         gFifoLvl1       => gConf.FifoLvl1
      )
      port map (
         CLK               => CLK,
         nRESET            => nRESET,
         nCS               => nCS,
         nRead             => nRead,
         nWrite            => nWrite,
         Datain            => Datain,
         Addr              => Addr,
         TX_in             => TX,
         TxBusy            => TxBusy,
         TxEmpty           => TxEmpty,
         fifo_tx_empty     => fifo_tx_empty,
         fifo_rx_empty     => fifo_rx_empty,
         fifo_rx_full      => fifo_rx_full,
         ReceivedParityErr => ParityErr,
         ReceivedFrameErr  => FrameErr,
         ReceivedBreak     => Break,
         DataReceived      => DataReceived,
         TimeOut           => TimeOut,
         DataRX            => DataFifoRX,
         DataOut           => Dataout,
         nIRQ              => nINT,
         TxD               => TxD,
         write_fifo_tx     => write_fifo_tx,
         N8Bits            => N8Bits,
         ParityEn          => ParityEn,
         EvenPar           => EvenPar,
         NbStop            => NbStop,
         divider           => divider,
         reset_fifo_rx     => reset_fifo_rx,
         reset_fifo_tx     => reset_fifo_tx,
         hard_flow         => hard_flow,
         Tick              => Tick,
         DataTX            => DataTX,
         TimeOutDur        => TimeOutDur,
         nCTS              => nCTS,
         nDCD              => nDCD,
         nDSR              => nDSR,
         nRI               => nRI,
         nDTR              => nDTR,
         nRTS              => nRTS,
         CTS_reg           => CTS_reg,
         LineFree          => LineFree,
         NumWordsReceived  => CntNumWords,
         read_fifo_rx      => read_fifo_rx,
         fifo_rx_empty_lat => fifo_rx_empty_lat,
         fifo_tx_full      => fifo_tx_full,
         CLK_COM           => CLK_COM,
         GetNextData       => GetNextData,
         write_fifo_rx     => write_fifo_rx,
         AD_SYNC           => AD_SYNC
      );
   I_XMIT_SEQ : xmit_seq
      port map (
         CLK           => CLK,
         nRST          => nRESET,
         hard_flow     => hard_flow,
         CTS_reg       => CTS_reg,
         TxBusy        => TxBusy,
         fifo_tx_empty => fifo_tx_empty,
         rd_fifo_tx    => rd_fifo_tx,
         start_xmit    => start_xmit
      );

end struct;


-- renoir interface_end

-- **********************************************************************************
-- RCS revision  : $Revision: $
-- RCS history   : $Log$
-- **********************************************************************************
-- renoir header_start
-- **********************************************************************************
--  State Diagram generated by Mentor Graphics' Renoir(TM) 2002.1b (Build 7)
--   Library: infigo2
--  Copyright Schlumberger Systems, 2000-2001
-- **********************************************************************************
-- renoir header_end
--------------------------------------------------------------------
-- * file revision: $Revision: 1.1 $
-- * file history : $Log: fsm.sm,v $
-- * file history : Revision 1.1  2012/06/26 14:06:30  pschirrer
-- * file history : Initial version
-- * file history :
-- * file history : Revision 1.4  2002/04/30 05:46:08  gehin
-- * file history : Rewitten for speed and area optimization after synthesis.
-- * file history :
--
-- User comments:
--
--------------------------------------------------------------------
-- Generation properties:
--   Machine             :  "fsm_rx", synchronous
--   Encoding            :  none
--   Style               :  case, 2 processes
--   Clock               :  "CLK", rising 
--   Reset               :  "nRST", asynchronous, active low
--   Second Reset        :  [none], active low
--   State variable type :  [auto]
--   Recovery state      :  "<start_state>"
--   Default state assignment disabled
--   State actions registered on current state
--   
--   SIGNAL         SCOPE DEFAULT  RESET          STATUS 
--   Break          OUT            '0'            CLKD   
--   DataReceived   OUT   '0'      '0'            REG    
--   Dout           OUT            (others=>'0')  CLKD   
--   FrameErr       OUT            '0'            CLKD   
--   ParityErr      OUT            '0'            CLKD   
--   StartDetected  OUT   '0'      '0'            REG    
--   sBreak_in      INT            '0'            REG    
--   sClkDiv        INT            (others=>'0')  REG    
--   sClkRes        INT   '0'      '0'            REG    
--   sRX            INT            '0'            REG    
--   sRX1           INT            '0'            REG    
--   sRxBitCnt      INT            (others=>'0')  REG    
--   sRx_Buf        INT            (others=>'0')  REG    
--   sShift         INT            '1'            REG    
--   sTop1          INT   '0'      '0'            REG    
--   
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library shared_lib;
use shared_lib.pkg_defines.all;

architecture fsm of com_uart_rx_div is

   -- Architecture Declarations
   Signal sTop1     : Std_logic;                               -- Receiver clock
   Signal sRx_Buf   : Std_logic_vector (10 downto 0);  -- currently received word
   Signal sRxBitCnt : unsigned (3 downto 0);                  -- Received bit count
   Signal sClkDiv   : unsigned (2 downto 0);                   -- / 8 divider to obtain 2x baud rate
   Signal sClkRes   : Std_logic;                               -- Reset clock divider
   signal sRX : std_logic;
   signal sRX1 : std_logic;
   signal sShift : std_logic; -- shift /2 divider
   signal sBreak_in : std_logic;
   
   -----------------------------
   function make_parity -- this function compute parity
        (Din  : std_logic_vector;
         even : std_logic) return std_logic is
   variable vpar : std_logic;
   begin
     vpar := not even;
     for i in Din'range loop
       vpar := vpar xor Din(i);
     end loop;
     return vpar;
   end make_parity;
   -----------------------------

   type state_type is (
      sIdle,
      sLoad,
      sShifter,
      sStop
   );

   -- State vector declaration
   attribute state_vector : string;
   attribute state_vector of fsm : architecture is "current_state" ;


   -- Declare current and next state signals
   signal current_state : state_type ;
   signal next_state : state_type ;

   -- Declare any pre-registered internal signals
   signal Break_cld : std_logic  ;
   signal DataReceived_int : std_logic  ;
   signal Dout_cld : std_logic_vector (gConf.NDbits-1 downto 0) ;
   signal FrameErr_cld : std_logic  ;
   signal ParityErr_cld : std_logic  ;
   signal StartDetected_int : std_logic  ;

begin

   ----------------------------------------------------------------------------
   clocked : process(
      CLK,
      nRST
   )
   ----------------------------------------------------------------------------
   begin
      if (nRST = '0') then
         current_state <= sIdle;
         -- Reset Values
         Break_cld <= '0';
         DataReceived <= '0';
         Dout_cld <= (others=>'0');
         FrameErr_cld <= '0';
         ParityErr_cld <= '0';
         StartDetected <= '0';
         sBreak_in <= '0';
         sClkDiv <= (others=>'0');
         sClkRes <= '0';
         sRX <= '0';
         sRX1 <= '0';
         sRxBitCnt <= (others=>'0');
         sRx_Buf <= (others=>'0');
         sShift <= '1';
         sTop1 <= '0';
      elsif (CLK'event and CLK = '1') then
         current_state <= next_state;
         -- Registered output assignments
         DataReceived <= DataReceived_int;
         StartDetected <= StartDetected_int;

         -- Default Assignment To Internals
         sClkRes <= '0';
         sTop1 <= '0';
         -- Global Internal Actions
         sRX1 <= RX; --metastability avoidance
         sRX <= sRX1;
         
         -- RX Clock Generation
         -- sTop1 period is half time period
         
         if sClkRes='1' then
            sClkDiv <= (others=>'0');
         else
           if Tick='1' then
              sClkDiv <=  sClkDiv + 1;
              if sClkDiv = unsigned(divider(3 downto 1)) then
                 sClkDiv <= (others=>'0');
                 sTop1 <= '1';
              end if;
           end if;
         end if;

         -- Combined Actions for internal signals only
         case current_state is
         when sIdle =>
            -- keep resetting internal tick generator
            -- this will force it to be synchronized after each start bit
            sClkRes <= '1';
            FrameErr_cld <= '0';
            ParityErr_cld <= '0';
            Break_cld <= '0';
         when sLoad =>
            -- compute total number of bits to receive for a word
            if N8Bits='0' and ParityEn='0' then
               sRxBitCnt <= X"9"; -- 9 bits
            elsif N8Bits='0' and ParityEn='1' then
               sRxBitCnt <= X"A"; -- 10 bits
            elsif N8Bits='1' and ParityEn='0' then
               sRxBitCnt <= X"A"; -- 10 bits
            elsif N8Bits='1' and ParityEn='1' then
               sRxBitCnt <= X"B"; -- 11 bits
            end if;
            -- allow first sTop1 tick to shift data
            sShift <= '1';
            sBreak_in <= '1'; -- suppose this is a Break
         when sShifter =>
            if sTop1='1' then
               sShift <= not sShift;
               if sShift='1' then
                  -- if a data bit is not equal to '0' , 
                  -- this was NOT a Break     
                  if sRX='1' then 
                     sBreak_in <= '0'; 
                  end if;
                  sRxBitCnt <= sRxBitCnt - 1;
                  sRX_Buf(10) <= sRx;
                  for i in 0 to 9 loop -- data shifter
                     sRx_Buf(i) <= sRx_Buf(i+1);
                  end loop;
               end if;
            end if;
         when sStop =>
            -- extract data bits and compute error
            Break_cld <= sBreak_in;
            if N8Bits='0' and ParityEn='0' then -- 7,N,1
               Dout_cld <= '0' & sRX_buf(9 downto 3);
               FrameErr_cld <=  sRX_Buf(2) or (not sRX_Buf(10));
               ParityErr_cld <= '0';
            elsif N8Bits='0' and ParityEn='1' then -- 7,P,1
               Dout_cld <= '0' & sRX_buf(8 downto 2);
               FrameErr_cld <=  sRX_Buf(1) or (not sRX_Buf(10));
               ParityErr_cld <= sRX_Buf(9) xor make_parity('0' & sRX_Buf(8 downto 2),EvenPar);
            elsif N8Bits='1' and ParityEn='0' then -- 8,N,1
               Dout_cld <= sRX_buf(9 downto 2);
               FrameErr_cld <=  sRX_Buf(1) or (not sRX_Buf(10));
               ParityErr_cld <= '0';
            elsif N8Bits='1' and ParityEn='1' then -- 8,P,1
               Dout_cld <= sRX_buf(8 downto 1);
               FrameErr_cld <=  sRX_Buf(0) or (not sRX_Buf(10));
               ParityErr_cld <= sRX_Buf(9) xor make_parity(sRX_Buf(8 downto 1),EvenPar);
            end if;
         when others =>
            null;
         end case;

      end if;

   end process clocked;

   ----------------------------------------------------------------------------
   nextstate : process (
      current_state,
      sRX,
      sRX1,
      sRxBitCnt
   )
   ----------------------------------------------------------------------------
   begin
      -- Default Assignment
      DataReceived_int <= '0';
      StartDetected_int <= '0';
      -- Default Assignment To Internals

      -- Combined Actions
      case current_state is
      when sIdle =>
         if (--wait for falling edge (start bit) on serial line
         sRX='1' and sRX1='0'
         ) then
            next_state <= sLoad;
         else
            next_state <= sIdle;
         end if;
      when sLoad =>

         StartDetected_int <= '1'; -- start bit detected
            next_state <= sShifter;
      when sShifter =>
         if (sRxBitCnt=X"0") then
            next_state <= sStop;
         else
            next_state <= sShifter;
         end if;
      when sStop =>

         -- Note:
         -- sRx_Buf receive buffer content based on communication format :
         -- (T : sTop bit, 0..7 : data bit , S : Start bit, P : Parity bit)
         -- second stop bit is never checked
         -- bit    10 9 8 7 6 5 4 3 2 1 0 
         -- 8,N,1 : T 7 6 5 4 3 2 1 0 S -  
         -- 8,P,1 : T P 7 6 5 4 3 2 1 0 S
         -- 7,N,1 : T 6 5 4 3 2 1 0 S - - 
         -- 7,P,1 : T P 6 5 4 3 2 1 0 S -   
         
         -- signal that a data has been received
         DataReceived_int <= '1';
            next_state <= sIdle;
      when others =>
         next_state <= sIdle;
      end case;

   end process nextstate;

   -- Concurrent Statements
   -- Clocked output assignments
   Break <= Break_cld;
   Dout <= Dout_cld;
   FrameErr <= FrameErr_cld;
   ParityErr <= ParityErr_cld;


end fsm;

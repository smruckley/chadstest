
-- hds interface_end
architecture synth of pwm_io is

	signal	sPowerOut	: std_logic_vector(7 downto 0);
	signal	sResetOut	: std_logic_vector(7 downto 0);
	signal	sCounter	: natural range 0 to gDivider;
	signal	sDoReset	: std_logic;
	signal	sPwmOut		: std_logic;
	signal	sEnable		: std_logic;
	signal	sUseClk		: std_logic;
	signal	sPwmVal		: std_logic_vector (7 downto 0);
	signal	sPwmReg		: unsigned(7 downto 0);
	signal	sInClk1		: std_logic;
	signal	sInClk2		: std_logic;
	signal	sSlowClk	: std_logic;
	signal	sTick		: std_logic;
	
begin

	Output   <= sPowerOut xor not gOutPolarity;
	Resetout <= sResetout xor not gRstPolarity;
	pwm_out <= sPwmOut;

	with Addr select
		Dataout <= sPowerOut                when "000",
		           sResetOut                when "001",
				   "000000"&sUseClk&sEnable when "010",
		           sPwmVal                  when "011",
				   X"A5"                    when "100",		-- Test read
				   (others => '0')          when others;
	
io_port:	process (nRESET, CLK)
	begin
		if nRESET = '0' then
			sPowerOut <= (others => '0');
			sResetout <= (others => '0');
			sPwmOut   <= '0';
--RoyB			sEnable   <= '0';
--RoyB			sUseClk   <= '0';
--RoyB			sPwmVal   <= (others => '0');
			sEnable   <= '1';
			sUseClk   <= '1';
			sPwmVal   <= X"80";
			sPwmReg   <= (others => '0');
			sDoReset  <= '0';
		elsif Rising_Edge(CLK) then
			if nCS = '0' and nWrite = '0' then	-- Register access
				case AD_SYNC is
					when "000" =>
						sPowerOut <= Datain;
					when "001" =>
						sResetOut <= Datain;
						sDoReset  <= '1';
					when "010" =>
						sEnable   <= Datain(0);
						sUseClk   <= Datain(1);
					when "011" =>
						sPwmVal   <= Datain;
					when others =>
						NULL;
				end case;
			end if;
			if sDoReset = '1' or sEnable = '1' then
				if sTick = '1' then
					sPwmReg <= sPwmReg + 1;
					if sPwmReg < unsigned(sPwmVal) then
						sPwmOut <= sEnable;
					else
						sPwmOut <= '0';
						if sDoReset = '1' then
							sResetout <= (others => '0');
							sDoReset  <= '0';
						end if;
					end if;
				end if;
			else
				sPwmReg <= (others => '0');
				sPwmOut <= '0';
			end if;
		end if;
	end process;

tick_gen:	process (nRESET, sEnable, sDoReset, CLK)
	begin
		if nRESET = '0' or (sEnable = '0' and sDoReset = '0') then
			sInClk1  <= '0';
			sInClk2  <= '0';
			sSlowClk <= '0';
			sTick    <= '0';
			sCounter <= 0;
		elsif Rising_Edge(CLK) then
			sInClk1  <= Slow_CLK;
			sInClk2	 <= sInClk1;
			sSlowClk <= sInClk2;
			sTick    <= '0';
			if sUseClk = '1' or (sSlowClk = '0' and sInClk2 = '1') then
				sCounter <= sCounter + 1;
				if sCounter = gDivider - 1 then
					sTick    <= '1';
					sCounter <= 0;
				end if;
			end if;
		end if;
	end process;
end synth;


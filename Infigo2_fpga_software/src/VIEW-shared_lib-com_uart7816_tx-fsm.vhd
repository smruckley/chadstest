
-- renoir interface_end

-- **********************************************************************************
-- RCS revision  : $Revision: $
-- RCS history   : $Log$
-- **********************************************************************************
-- Describe below component summary:
-- Sections:
--    class    : enter device class (family) , example : communication, interrupt controller, pio, specific... 
--    top      : must be yes for a top component, else no
--    device   : summarize device functionnality, example : UART with RX/TX FIFO and flow control ...
--    status   : enter developpment status, examples : stable, developpment, experimental ...
-- 
-- @begin
-- [class]
-- not specified
-- [top]
-- not specified
-- [device]
-- not specified
-- [usage]
-- not specified
-- [status]
-- not specified
-- @end
-- 
-- --------------------------------------------------------------------
-- -- * file revision: $Revision: 1.1 $
-- -- * file history : $Log: fsm.sm,v $
-- -- * file history : Revision 1.1  2004/09/08 13:09:01  gehin
-- -- * file history : initial revision
-- -- * file history :
-- --
-- -- User comments:
-- --
-- --------------------------------------------------------------------
-- renoir header_start
-- **********************************************************************************
--  State Diagram generated by Mentor Graphics' Renoir(TM) 2002.1b (Build 7)
--   Library: shared_lib
--  Copyright Schlumberger Systems, 2000-2001
-- **********************************************************************************
-- renoir header_end
-- Generation properties:
--   Machine             :  "machine0", synchronous
--   Encoding            :  none
--   Style               :  case, 2 processes
--   Clock               :  "CLK", rising 
--   Reset               :  "nRST", asynchronous, active low
--   Second Reset        :  [none], active low
--   State variable type :  [auto]
--   Recovery state      :  "<start_state>"
--   Default state assignment disabled
--   State actions registered on current state
--   
--   SIGNAL             SCOPE DEFAULT  RESET          STATUS 
--   Tx                 OUT                           COMB   
--   TxBusy             OUT   '1'      '0'            REG    
--   TxEmpty            OUT                           COMB   
--   parity_error       OUT            '0'            CLKD   
--   resync_etugen      OUT   '0'      '0'            REG    
--   startbit           OUT                           COMB   
--   sCntRetry          INT            (others=>'0')  REG    
--   sRegDIN            INT            (others=>'0')  REG    
--   sStartbit          INT   '0'      '0'            REG    
--   sTSR               INT            (others=>'1')  REG    
--   sTX                INT            '1'            REG    
--   sTop1              INT                           COMB   
--   sTxBitCnt          INT            (others=>'0')  REG    
--   sTxEmpty           INT            '1'            REG    
--   startbit_signaled  INT            '0'            REG    
--   
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library shared_lib;
use shared_lib.pkg_defines.all;

architecture fsm of com_uart7816_tx is

   -- Architecture Declarations
   constant cStart : std_logic := '0'; -- start bit 
   constant cStop : std_logic := '1'; -- stop bit
   
   signal sRegDIN : std_logic_vector(DIN'HIGH downto 0);
   signal sTSR : std_logic_vector(9 downto 0); -- 1 start + 7/8 bits + 1 parity  => 10 bits max
   signal sTxBitCnt : unsigned(3 downto 0); -- bit counter
   --signal stick_div : std_logic;
   signal sTop1 : std_logic; -- baud rate tick generator
   signal sCntRetry : unsigned(2 downto 0); -- retry counter for character repetition
   signal startbit_signaled : std_logic;
   signal sStartbit : std_logic;
   
   signal sTxEmpty : std_logic;
   signal sTX :std_logic;
   
   
   -----------------------------
   function make_parity -- this function compute parity
        (Din  : std_logic_vector;
         even : std_logic) return std_logic is
   variable vpar : std_logic;
   begin
     vpar := not even;
     for i in Din'range loop
       vpar := vpar xor Din(i);
     end loop;
     return vpar;
   end make_parity;
   -----------------------------

   type state_type is (
      sIdle,
      sLoad,
      sShift,
      sCheck,
      sETU2,
      sWaitGT,
      sCrep
   );

   -- State vector declaration
   attribute state_vector : string;
   attribute state_vector of fsm : architecture is "current_state" ;


   -- Declare current and next state signals
   signal current_state : state_type ;
   signal next_state : state_type ;

   -- Declare any pre-registered internal signals
   signal TxBusy_int : std_logic  ;
   signal parity_error_cld : std_logic  ;
   signal resync_etugen_int : std_logic  ;

begin

   ----------------------------------------------------------------------------
   clocked : process(
      CLK,
      nRST
   )
   ----------------------------------------------------------------------------
   begin
      if (nRST = '0') then
         current_state <= sIdle;
         -- Reset Values
         TxBusy <= '0';
         parity_error_cld <= '0';
         resync_etugen <= '0';
         sCntRetry <= (others=>'0');
         sRegDIN <= (others=>'0');
         sStartbit <= '0';
         sTSR <= (others=>'1');
         sTX <= '1';
         sTxBitCnt <= (others=>'0');
         sTxEmpty <= '1';
         startbit_signaled <= '0';
      elsif (CLK'event and CLK = '1') then
         current_state <= next_state;
         -- Registered output assignments
         TxBusy <= TxBusy_int;
         resync_etugen <= resync_etugen_int;

         -- Default Assignment To Internals
         sStartbit <= '0';
         -- Global Internal Actions
         -- buffer register loading
         if LD='1' then
            sTxEmpty<='0';
            sRegDIN <= DIN;
         end if;

         -- Combined Actions for internal signals only
         case current_state is
         when sIdle =>
            sCntRetry <= "100"; -- set number of maximum retry upon parity error
         when sLoad =>
            sTxEmpty <= '1';
            parity_error_cld <= '0'; -- assume no parity error
            startbit_signaled <= '0';
            
            -- load data to transmit shift register and compute total number of bits to receive for a word
            if N8bits='0' then -- 7 bits
               if ParityEn='0' then -- no parity
                  sTxBitCnt <= X"9"; -- 9 bits
                  sTSR(8 downto 0) <= cStop & sRegDIN(6 downto 0) & cStart;
               else
                  sTxBitCnt <= X"A"; -- 10 bits
                  sTSR <= cStop & make_parity('0' & sRegDIN(6 downto 0),EvenPar) & sRegDIN(6 downto 0) & cStart;
               end if;
            else -- 8 bits
               if ParityEn='0' then -- no parity
                  sTxBitCnt <= X"9"; -- 9 bits
                  sTSR <= cStop & sRegDIN(7 downto 0) & cStart;
               else
                  sTxBitCnt <= X"B"; -- 10 bits +1 bit for parity check. T=0 mode only since it will lead to a minimum guard time of 12 ETUs
                  sTSR <= make_parity(sRegDIN,EvenPar) & sRegDIN(7 downto 0) & cStart;
               end if;
            end if;
         when sShift =>
            if sTop1='1' then
               if startbit_signaled='0' then
                  startbit_signaled <= '1'; -- indicate start bit, usefull for guard time counting
                  sStartbit <= '1';
               end if;   
               sTX <= sTSR(0);
               sTxBitCnt <= sTxBitCnt-1;
               sTSR <= '1' & sTSR (sTSR'high downto 1);  -- shifter 
            end if;
         when sCheck =>
            -- check parity error signaled by receiver
            parity_error_cld <= not RX;
         when sETU2 =>
            sCntRetry <= sCntRetry-1;
         when sWaitGT =>
            if indirect_conv='1' then -- indirect convension
              sTSR(1) <= not sTSR(8);
              sTSR(2) <= not sTSR(7);
              sTSR(3) <= not sTSR(6);
              sTSR(4) <= not sTSR(5);
              sTSR(5) <= not sTSR(4);
              sTSR(6) <= not sTSR(3);
              sTSR(7) <= not sTSR(2);
              sTSR(8) <= not sTSR(1);
            end if;
         when others =>
            null;
         end case;

      end if;

   end process clocked;

   ----------------------------------------------------------------------------
   nextstate : process (
      crep,
      current_state,
      guardtime_elapsed,
      parity_error_cld,
      sCntRetry,
      sTop1,
      sTxBitCnt,
      sTxEmpty
   )
   ----------------------------------------------------------------------------
   begin
      -- Default Assignment
      TxBusy_int <= '1';
      resync_etugen_int <= '0';
      -- Default Assignment To Internals

      -- Combined Actions
      case current_state is
      when sIdle =>

         TxBusy_int <='0';
         if (sTxEmpty='0') then
            next_state <= sLoad;
         else
            next_state <= sIdle;
         end if;
      when sLoad =>
            next_state <= sWaitGT;
      when sShift =>
         if (sTxBitCnt=X"F") then
            next_state <= sCheck;
         else
            next_state <= sShift;
         end if;
      when sCheck =>
            next_state <= sCrep;
      when sETU2 =>
         if (sCntRetry=0) then
            next_state <= sIdle;
         else
            next_state <= sLoad;
         end if;
      when sWaitGT =>
         if (guardtime_elapsed='1'
         -- wait until guardtime elapsed
         ) then
            next_state <= sShift;
         else
            next_state <= sWaitGT;
         end if;
      when sCrep =>
         if (crep='0' or (crep='1' and parity_error_cld='0')
         -- no parity error signaled by receiver
         -- or we do not want auto-retry
         ) then
            next_state <= sIdle;
         elsif (sTop1='1'
         -- first ETU delay as specified
         -- by ISO7816-3 standard 
         -- (2 ETUs before re-emission,
         -- second ETU will be 'added' 
         -- during sTop1 waiting at state sShift)
         ) then
            next_state <= sETU2;
         else
            next_state <= sCrep;
         end if;
      when others =>
         next_state <= sIdle;
      end case;

   end process nextstate;

   -- Concurrent Statements
   -- Clocked output assignments
   parity_error <= parity_error_cld;

TX <= sTX;
TxEmpty <= sTxEmpty;
startbit <= sStartbit;
sTop1 <= tick;

end fsm;

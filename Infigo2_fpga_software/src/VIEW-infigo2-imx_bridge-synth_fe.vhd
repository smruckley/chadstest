
-- hds interface_end
architecture synth_fe of imx_bridge is

	signal	snOE_reg	: std_logic;
	signal	snWR_reg	: std_logic;
	signal	snCS_reg	: std_logic;
	signal	snOE_reg1	: std_logic;
	signal	snWR_reg1	: std_logic;
	signal	snWR_reg2	: std_logic;
	signal	snCS_reg1	: std_logic;
	signal	sDataEn		: std_logic;
--	signal	sInt_addr	: std_logic_vector(4 downto 0);
	signal	sAddrH		: std_logic_vector(Addr_H'RANGE);
--	signal	sAddrH1		: std_logic_vector(Addr_H'RANGE);
--	signal	snInt_CS	: std_logic_vector(nCSO'RANGE);
	signal  sDataHost	: std_logic_vector(7 downto 0);
	
begin

--	sInt_addr <= snCS_reg & sAddrH;
	sDataEn   <= nCS or nOE or Addr_H(3);
	DataIn    <= (others => 'Z') when sDataEn = '1' else sDataHost;		-- Top level tristate for data bus
	with Addr_H select													-- Asynchronous read
		sDataHost <= DMI0  when "0000",
					 DMI1  when "0001",
					 DMI2  when "0010",
					 DMI3  when "0011",
					 DMI4  when "0100",
					 DMI5  when "0101",
					 DMI6  when "0110",
					 DMI7  when "0111",
					 X"00" when others;
					 
--	with sInt_addr select												-- Chip select generation
--		snInt_CS <= "11111110" when "00000",
--		            "11111101" when "00001",
--		            "11111011" when "00010",
--		            "11110111" when "00011",
--		            "11101111" when "00100",
--		            "11011111" when "00101",
--		            "10111111" when "00110",
--		            "01111111" when "00111",
--		            "11111111" when others;

	process (nRESET, CLK)
	begin
		if nRESET = '0' then
			snOE_reg  <= '1';
			snWR_reg  <= '1';
			snCS_reg  <= '1';
			snOE_reg1 <= '1';
			snWR_reg1 <= '1';
			snWR_reg2 <= '1';
			snCS_reg1 <= '1';
			nRead     <= '1';
			nWrite    <= '1';
--			sAddrH1   <= (others => '0');
--			sAddrH    <= (others => '0');
			Dataout   <= (others => '0');
			nCSO      <= (others => '1');
		elsif Falling_Edge (CLK) then
			nRead     <= '1';
			nWrite    <= '1';
			snOE_reg1 <= nOE;							-- Metastability avoidance
			snWR_reg1 <= R_nW;
			snCS_reg1 <= nCS;
			snOE_reg  <= snOE_reg1;
			snWR_reg2 <= snWR_reg1;
			snWR_reg  <= snWR_reg2;
			snCS_reg  <= snCS_reg1;
--			sAddrH1   <= Addr_H;
--			sAddrH    <= sAddrH1;
			nCSO      <= (others => '1');
			if snOE_reg1 = '1' and snOE_reg = '0' and snCS_reg = '0' then	-- Detect end of read cycle
				nRead <= '0';							-- Generate read tick
			end if;
			if snWR_reg2 = '0' and snWR_reg = '1' then	-- Detect start of write cycle
				nWrite  <= '0';							-- Generate write tick
				Dataout <= Datain;						-- Latch data
			end if;
			if snCS_reg = '0' then
--				nCSO  <= snInt_CS;
				case sAddrH is
					when "0000" =>
						nCSO(0) <= '0';
					when "0001" =>
						nCSO(1) <= '1';
					when "0010" =>
						nCSO(2) <= '0';
					when "0011" =>
						nCSO(3) <= '0';
					when "0100" =>
						nCSO(4) <= '0';
					when "0101" =>
						nCSO(5) <= '0';
					when "0110" =>
						nCSO(6) <= '0';
					when "0111" =>
						nCSO(7) <= '0';
					when others =>
						NULL;
				end case;
			end if;
		end if;
	end process;

	process (nRESET, nCS)
	begin
		if nRESET = '0' then
			AddrO     <= (others => '0');
			sAddrH    <= (others => '0');
		elsif nCS = '0' then
			AddrO     <= Addr_L and "00000111";			-- Latch addresses
			sAddrH    <= Addr_H;
		end if;
	end process;
	
end synth_fe;


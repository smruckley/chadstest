-- --------------------------------------------------------------------
-- >>>>>>>>>>>>>>>>>>>>>>>>> COPYRIGHT NOTICE <<<<<<<<<<<<<<<<<<<<<<<<<
-- --------------------------------------------------------------------
-- Copyright (c) 2005 by Lattice Semiconductor Corporation
-- --------------------------------------------------------------------
--
--
--                     Lattice Semiconductor Corporation
--                     5555 NE Moore Court
--                     Hillsboro, OR 97214
--                     U.S.A.
--
--                     TEL: 1-800-Lattice  (USA and Canada)
--                          1-408-826-6000 (other locations)
--
--                     web: http://www.latticesemi.com/
--                     email: techsupport@latticesemi.com
--
-- --------------------------------------------------------------------
--
-- Simulation Library File for MACHXO2
--
-- $Header:  
--
 
--
----- PACKAGE global -----
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;

PACKAGE global IS
	SIGNAL gsrnet: std_logic := 'H';
	SIGNAL purnet: std_logic := 'H';
 	SIGNAL tsallnet: std_logic := 'H';
	
END global;


PACKAGE BODY global IS 
END global;


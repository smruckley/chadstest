
-- hds interface_end

------------------------------------------------------------------
--* file revision: $Revision: 1.1 $
--* file history : $Log: synth_ram_sync_dp_generic.vhd,v $
--* file history : Revision 1.1  2012/06/26 14:02:43  pschirrer
--* file history : Initial version
--* file history :
--* file history : Revision 1.11  2008/10/20 08:55:15  igehin
--* file history : + RF engine :
--* file history : - re-enabled Type B Frame Guard Time error check in reception to comply  with RCTIF test
--* file history : - TR1 timing checks modified 02/04/2008 upon A. Maurel request for RCTIF test :   
--* file history :   10 ETUs < TR1 < 25 ETUs
--* file history : - reception engine changed to allow 1/8 ETUs resolution
--* file history : 
--* file history : + SAM ART : Set to  2ms instead of 200 us for time 't2': patched on 23/01/2008 to reflect calypso changes.
--* file history :   See  � #005 from calypso technical note from Spirtech on 30 April 2002
--* file history :
--* file history : Revision 1.10  2002/04/29 11:49:43  gehin
--* file history : Modified generic declration (moved to pkg.conf package).
--* file history :
--* file history : Revision 1.9  2001/12/03 17:09:27  gehin
--* file history : Added RCS keywords for block interfaces.
--* file history :
--* file history : Revision 1.8  2001/11/15 09:32:40  gehin
--* file history : Changed title block on symbol. Also Clean up sources.
--* file history :

-- User comments:

-- Generic RAM inference
-- Implemented as BLOCK RAM for Xilinx SPARTAN2 and Altera ACEX,FLEX devices
-- Implemented as distributed RAM for Xilinx SPARTAN & SPARTAN XL devices
-- Read access time is 2 clock period.
----------------------------------------------------------------------------

ARCHITECTURE synth_ram_sync_dp_generic OF ram_sync_dp IS
   type     memory is array(2**addr_SIZE-1 downto 0) of std_logic_vector(DataIn_SIZE-1 downto 0);
   signal   RAM : memory;

BEGIN
  process(CLK)
     begin
      if Rising_Edge(CLK) then
         DataOut <= RAM(to_integer(unsigned(rd_addr))); -- data output is registered 
         if (WRCYCLE='1' and CS='1') then
               RAM(to_integer(unsigned(wr_addr))) <= DataIn; 
            end if;
         end if;
  end process;

END synth_ram_sync_dp_generic;

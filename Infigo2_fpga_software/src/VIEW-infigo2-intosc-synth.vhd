
-- hds interface_end
architecture synth of intosc is

	COMPONENT OSCH
	-- synthesis translate_off
		GENERIC (NOM_FREQ: string := "2.56");
	-- synthesis translate_on
		PORT (	STDBY		: IN  std_logic;
				OSC			: OUT std_logic;
				SEDSTDBY	: OUT std_logic);
	END COMPONENT;
		attribute NOM_FREQ : string;
		attribute NOM_FREQ of OSCinst0 : label is SET_FREQ;

begin

	OSCInst0: OSCH
	-- synthesis translate_off
		GENERIC MAP (NOM_FREQ => SET_FREQ)
	-- synthesis translate_on
		PORT MAP (	STDBY    => stdby,
					OSC      => osc,
					SEDSTDBY => sedstdby
				 );

end synth;


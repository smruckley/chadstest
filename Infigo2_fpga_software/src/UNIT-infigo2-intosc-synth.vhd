-- Describe below component summary:
-- Sections:
--    class    : enter device class (family) , example : communication, interrupt controller, pio, specific...
--    top      : must be yes for a top component, else no
--    device   : summarize device functionnality, example : UART with RX/TX FIFO and flow control ...
--    status   : enter developpment status, examples : stable, developpment, experimental ...
--
-- @begin
-- [class]
-- CLOCK
-- [top]
-- NO
-- [device]
-- MACH_XO2 FAMILY INTERNAL CLOCK MODULE
-- [usage]
-- MACH_XO2 FAMILY DESIGN
-- [status]
-- UNDER DEVELOPMENT
-- @end
--
-- --------------------------------------------------------------------
-- -- * file revision: $Revision: 1.2 $
-- -- * file history : $Log: synth.vhd,v $
-- -- * file history : Revision 1.2  2012/06/26 13:47:43  pschirrer
-- -- * file history : Initial version (corrected title bloc comments)
-- -- * file history :
-- --
-- -- User comments:
-- --
-- --------------------------------------------------------------------
-- renoir header_start
-- **********************************************************************************
-- VHDL combined entity/architecture  generated by Mentor Graphics' Renoir(TM) 2002.1b (Build 7)
--   Library: lattice_MachXO2
--   Unit: osch
--   View: synth
--  Copyright Schlumberger Systems, 2000-2001
-- **********************************************************************************
-- renoir header_end

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

-- synthesis translate_off
library machxo2;
use machxo2.components.all;
-- synthesis translate_on


entity intosc is
   generic( 
      SET_FREQ : string := "53.20"
   );
   port( 
      stdby    : in     std_logic;
      osc      : out    std_logic;
      sedstdby : out    std_logic
   );

-- Declarations

end intosc ;


-- hds interface_end
architecture synth of com_uart7816_clkgen is
   signal sCnt_clk_icc     : unsigned(ICC_DIV'range);
   signal sCnt_tick_HETU   : unsigned(HETU_DIV'range);

   signal sCLK_ICC         : std_logic;
   signal scomp_cycle      : std_logic;
   signal stick_div        : std_logic;
   signal sTick_HETU       : std_logic;
   
   signal CLK_ICC_reg0,CLK_ICC_reg1 : std_logic;
   signal stick_HCLKICC    : std_logic;
   
   
begin
   
   CLK_ICC     <= sCLK_ICC WHEN (select_ext_clk_icc='0' and nRST='1') else 'Z';
   Tick_HETU   <= sTick_HETU;
   
   process(nRST,clk)
      begin
         if nRST='0' then
            sCnt_clk_icc      <= (others=>'0');
            sCnt_tick_HETU    <= (others=>'0');
            sCLK_ICC          <= '0';
            sTick_HETU        <= '0';
            scomp_cycle       <= '1';
            stick_HCLKICC     <= '0';
            tick_CLKICC       <= '0';
            stick_div         <= '0';
            Tick_ETU          <= '0';
            CLK_ICC_reg0      <= '0';
            CLK_ICC_reg1      <= '0';
            
         elsif rising_edge(clk) then

            CLK_ICC_reg0      <= CLK_ICC;
            CLK_ICC_reg1      <= CLK_ICC_reg0;

            -- ICC clock generation
            tick_CLKICC       <= '0';
            stick_HCLKICC     <= '0';
            
            if select_ext_clk_icc='0' then -- internal clock
               if en_clk_icc='1' then  -- clock generation enabled
                  sCnt_clk_icc <= sCnt_clk_icc -1;
                  if sCnt_clk_icc= 0 then
                     sCnt_clk_icc    <= unsigned(ICC_DIV); -- reload dividing counter
                     sCLK_ICC        <= not sCLK_ICC; -- generate 50% duty cicle clock
                     stick_HCLKICC   <= '1';          -- generate a tick each half ICC clock period
                     tick_CLKICC     <= sCLK_ICC;     -- generate a tick each ICC clock period
                  end if;
               else -- clock generation disabled
                  sCnt_clk_icc <= (others=>'0');
                  sCLK_ICC     <= '0';
               end if;
             else -- external ICC clock 
               if en_clk_icc='1' then -- clock sampling enabled
                  if CLK_ICC_reg0='0'  and CLK_ICC_reg1='1' then -- generate a tick on rising_edge
                     tick_CLKICC       <= '1';
                  end if;
               end if;
             end if;
            
            -- Half ETU generation
            sTick_HETU     <= '0';
            sCnt_tick_HETU <= sCnt_tick_HETU -1;

            if sCnt_tick_HETU = 1 and HETU_compens='1' and scomp_cycle='1' then -- compensation mode
               scomp_cycle <= '0';
               sTick_HETU <= '1';
               sCnt_tick_HETU <= unsigned(HETU_DIV);
            elsif sCnt_tick_HETU = 0 then
               scomp_cycle <= '1';
               sTick_HETU <= '1';
               sCnt_tick_HETU <= unsigned(HETU_DIV);
            end if;

            tick_ETU <= '0';
            if sTick_HETU='1' then
               stick_div <= not stick_div;
               if stick_div='0' then
                  tick_ETU <= '1';
               end if;
            end if;               

            if HETU_resync='1' then -- ETU resynchronization request
               sTick_HETU        <= '0';
               tick_ETU          <= '0';
               sCnt_tick_HETU    <= unsigned(HETU_DIV);
               stick_div         <= '0';
               scomp_cycle       <= '1';
            end if;


         end if;
      end process;
      
end synth;


